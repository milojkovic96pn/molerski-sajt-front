/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}","./public/index.html"
    ],
    safelist: [
        {
            pattern: /grid-cols-./,
            variants: ['xl'],
        }
    ],
    theme: {
        extend: {
            colors: {
                mainColor: "#a36d5e",
                mediumOrange: "#f1a33133",
                lightOrange: "#f1a3311a",
                mainBlue: "#193957",
                darkGreen: "#477d53",
                lightGreen: "#48A860",
                lightRed: "#F04343",
                grayColor: "#c6c6c6",
                lightGray: "#e5e5e5",
                lightYellow: "#F0CA43",
                orange: "#fcecd5",
            },
            backgroundImage: {
                loginBg: "url('assets/images/authenticationPage/bgLogo.png')",
            },
            fontFamily: {
                poppins: ["Poppins", "sans-serif"],
                dmSans: ["DM Sans", "sans-serif"],
            },
            screens: {
                sm: "640px",
                "sm-max": { max: "640px" },
                md: "768px",
                "md-max": { max: "768px" },
                "md-min": { min: "769px" },
                lg: "1024px",
                "lg-max": { max: "1024px" },
                xl: "1280px",
                "xl-max": { max: "1280px" },
                "xl-min": { min: "1281px" },
                "2xl": "1536px",
                "2xl-max": { max: "1536px" },
            },
        },
    },
    plugins: [],
}