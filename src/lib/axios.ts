import Axios from "axios";

import storage from "../utils/storage";
import {API_URL} from "../config";
const authRequestInterceptor = (config: any) => {
  const [token] = storage.getToken();

  if (config?.headers) {
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    config.headers["Access-Control-Allow-Origin"] = "*";
    config.headers["Access-Control-Allow-Headers"] = "*";
    config.headers["Access-Control-Allow-Methods"] =
      "GET,PUT,POST,DELETE,PATCH,OPTIONS";
    config.headers.Accept = "application/json, blob";
    return config;
  }
};

export const axios = Axios.create({
  baseURL: API_URL,
});

axios.interceptors.request.use(authRequestInterceptor);
axios.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    return Promise.reject(error);
  }
);
