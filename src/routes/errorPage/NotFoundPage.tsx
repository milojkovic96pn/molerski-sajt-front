import React from "react";
import {Button} from "../../components/elements/button/Button";
import {useNavigate} from "react-router-dom";

export const NotFoundPage = () => {

    const navigate = useNavigate();

    return (
        <div className="bg-orange">
            <div
                className="error-page flex flex-col lg:flex-row items-center justify-center h-screen text-center lg:text-left"
            >
                <div className="text-mainColor mt-10 lg:mt-0 xl:min-w-[500px]">
                    <div className="intro-x text-6xl font-medium">404</div>
                    <div className="intro-x text-xl lg:text-3xl font-medium">
                        Oops. Ova stranica ne postoji
                    </div>
                    <Button
                        className={"intro-x button button--lg mt-10 md-min:!w-[250px]"}
                        onClick={() => navigate("/")}
                    >
                        Vrati se na pocetnu
                    </Button>
                </div>
            </div>
        </div>
    )
}