import React from "react";
import {Navigate, Outlet, Route, Routes} from "react-router-dom";
import {lazyImport} from "../utils/lazyImport";

const { MainLayout } = lazyImport(
    () => import("../components/layout/MainLayout"),
    "MainLayout"
);
const { Dashboard } = lazyImport(
    () => import("../features/dashboard"),
    "Dashboard"
);
const { Calculator } = lazyImport(
    () => import("../features/calculator"),
    "Calculator"
);
const { NotFoundPage } = lazyImport(
    () => import("../routes/errorPage/NotFoundPage"),
    "NotFoundPage"
);
const { AboutUs } = lazyImport(
    () => import("../features/aboutUs"),
    "AboutUs"
);
const { Register } = lazyImport(
    () => import("../features/register"),
    "Register"
);
const { LoginModal } = lazyImport(
    () => import("../components/layout/components/LoginModal"),
    "LoginModal"
);
const { Users } = lazyImport(
    () => import("../features/users"),
    "Users"
);

const Layout = () => {
    return (
        <MainLayout>
            <React.Suspense fallback={null}>
                <Outlet />
                <LoginModal />
            </React.Suspense>
        </MainLayout>
    );
}

export const Protected = () => {

    return (
        <Routes>
            <Route path="" element={<Layout />}>
                {/*<Route index element={client ? <Dashboard /> : <Navigate to={"/poslodavci"} />} />*/}
                <Route index element={<Dashboard />} />
                <Route path={"kalkulator"} element={<Calculator />} />
                <Route path={"o-nama"} element={<AboutUs />} />
                <Route path={"za-majstore"} element={<Register />} />
                <Route path={"korisnici"} element={<Users />} />
            </Route>
            <Route path="*" element={<NotFoundPage />} />
        </Routes>
    );
};
