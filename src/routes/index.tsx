import {Route, Routes, useNavigate} from "react-router-dom";
import { lazyImport } from "../utils/lazyImport";
import {useEffect} from "react";
import {themeStore} from "../stores/theme";
import {authStore} from "../stores/authUser";
import {useProgressBar} from "../stores/progressBar";
import storage from "../utils/storage";
const { Protected } = lazyImport(
    () => import("./Protected"),
    "Protected"
);
export const AppRoutes = () => {

    const { user } = authStore()
    const { showTheme } = themeStore()
    const navigate = useNavigate();
    const { progress,setProgress } = useProgressBar();
    const [token] = storage.getToken();

    useEffect(() => {
        document.body.className = !showTheme ? "light " : "dark"
    }, [showTheme]);

    // useEffect(() => {
    //     if(!user) {
    //         navigate("/")
    //     }
    // }, [user,navigate])

    useEffect(() => {
        if(token && progress === "start") {
            setProgress("finish")
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    return (
        <Routes>
            <Route path="/*" element={<Protected />} />
        </Routes>
    );
};
