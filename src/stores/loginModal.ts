import create from "zustand";

export type Props = {
  loginModal: boolean;
  setLoginModal: (val: boolean) => void;
};

export const loginModalStore = create<Props>((set) => ({
  loginModal: false,
  setLoginModal: (val) =>
    set(() => ({
      loginModal: val,
    })),
}));
