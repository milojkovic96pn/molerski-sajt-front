import create from "zustand";
import { persist, devtools } from "zustand/middleware";

type Props = {
    showTheme: boolean;
    changeTheme: (value: boolean) => void;
};
export const themeStore = create(
    devtools(
        persist<Props>(
            (set) => ({
                showTheme: false,
                changeTheme: (value) =>
                    set(() => ({
                        showTheme: value,
                    })),
            }),
            {
                name: "changetheme",
                getStorage: () => localStorage,
            }
        )
    )
);
