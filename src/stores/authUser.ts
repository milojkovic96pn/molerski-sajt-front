import create from "zustand";
import { persist, devtools } from "zustand/middleware";

type AuthOptions = {
    user: any | null;
    setUser: (user: any) => void;
    clearUser: () => void;
};

export const authStore = create(
    devtools(
        persist<AuthOptions>(
            (set) => ({
                user: {} as any,
                setUser: (values) =>
                    set(() => ({
                        user: values,
                    })),
                clearUser: () =>
                    set(() => ({
                        user: null,
                    })),
            }),
            {
                name: "molerska_",
                getStorage: () => localStorage,
            }
        )
    )
);
