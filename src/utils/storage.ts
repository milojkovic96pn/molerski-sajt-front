const storage = {
  getToken: () => {
    return [
      JSON.parse(window.localStorage.getItem(`app_token`) as string),
      JSON.parse(window.localStorage.getItem(`app_refresh_token`) as string),
    ];
  },
  setToken: (token: string, refresh_token: string) => {
    if (token) window.localStorage.setItem(`app_token`, JSON.stringify(token));
    if (refresh_token)
      window.localStorage.setItem(`app_refresh_token`, JSON.stringify(token));
  },
  clearToken: () => {
    window.localStorage.removeItem(`app_token`);
    window.localStorage.removeItem(`app_refresh_token`);
  },
};

export default storage;
