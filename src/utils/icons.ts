import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faEye,
  faEyeSlash,
  faClipboard,
  faTimes,
  faBars,
  faCaretDown,
  faCaretUp,
  faUser,
  faClock,
  faRightFromBracket,
  faSortUp,
  faSortDown,
  faSort,
  faAngleRight,
  faAngleLeft,
  faAngleDoubleRight,
  faAngleDoubleLeft,
  faSearch,
  faFilter,
  faPlusCircle,
  faFileExcel,
  faCircleXmark,
  faUserCheck,
  faUserXmark,
  faCalendar as faCalendarSolid,
  faArrowRight,
  faNetworkWired,
  faArrowLeft,
  faCircleInfo,
  faPen,
  faCheck,
  faTrash,
  faFolderOpen,
  faQuestion,
  faFile,
  faFileLines,
  faCirclePlus,
  faCircleMinus,
  faRotateLeft, faUsers, faBookOpen,
    faCheckCircle
} from "@fortawesome/free-solid-svg-icons";

import {
  faCalendar, faFileWord

} from "@fortawesome/free-regular-svg-icons";

library.add(
  faClipboard,faEye,faEyeSlash,faTimes,faBars,faCaretDown,faCaretUp,faUser,faClock,
  faRightFromBracket,faSortUp,faSortDown,faSort, faAngleRight,faAngleLeft,faAngleDoubleRight,faAngleDoubleLeft,faSearch,faFilter,faPlusCircle,
  faFileExcel,faCalendar,faCircleXmark,faUserCheck,faUserXmark,faCalendarSolid,faArrowRight,faNetworkWired,faArrowLeft,faCircleInfo,faPen,faCheck,
  faTrash,faFolderOpen,faQuestion,faFile,faFileLines,faCirclePlus,faCircleMinus,faFileWord,faRotateLeft,faUsers,faBookOpen,faCheckCircle
);
