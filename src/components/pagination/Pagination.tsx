import Pagination from "react-js-pagination";
import {FunctionComponent, useState} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import {useTable} from "../../hooks/useTable";
import {InputField} from "../elements/input/InputField";
import {Button} from "../elements/button/Button";
import {PaginationType} from "./type/paginationType";
import Select from "react-select";
import {useMedia} from "react-use";
const CustomPagination : FunctionComponent<PaginationType> = props => {

  const { total, isEmpty, defaultPerPage, isFetching } = props
  const [searchParams] = useSearchParams();
  const { handlePageChange, handlePerPageChange } = useTable()
  const [page, setPage] = useState<number | null>(null);
  const [perPage, setPerPage] = useState<number | null>(null);
  const [ goToPage, setGoToPage ] = useState<string>("")
  const isMobile = useMedia("(max-width: 768px)");

  useEffect(() => {
    setPage(Number(searchParams.get("currentPage")) || 1);
    setPerPage(Number(searchParams.get("perPage")) || defaultPerPage || 15);
  }, [defaultPerPage, searchParams]);

  useEffect(() => {
    if (page && isEmpty && !isFetching && page > 1) handlePageChange(page);
  }, [isEmpty, handlePageChange, page, isFetching]);

  const handlePerPageSelect = (value:any) => {
    handlePageChange(1);
    handlePerPageChange(Number(value.value));
  };

  const changeGoToPage = (value:string) => {
    const rules = /^[0-9\b]+$/;

    if (rules.test(value) || value === "") {
      setGoToPage(value)
    }
  }

  const options = [
    { value: '5', label: '5' },
    { value: '10', label: '10' },
    { value: '15', label: '15' },
    { value: '25', label: '25' },
    { value: '50', label: '50' },
    { value: '100', label: '100' }
  ]

  return page && perPage && total && total > perPage ? (
      <div className="flex md-max:flex-col p-3 items-center justify-center bg-mediumOrange rounded-[10px] shadow-xl">
        <Pagination
            hideDisabled
            firstPageText={
              <FontAwesomeIcon icon={["fas", "angle-double-left"]} />
            }
            lastPageText={
              <FontAwesomeIcon icon={["fas", "angle-double-right"]} />
            }
            prevPageText={<FontAwesomeIcon icon={["fas", "angle-left"]} />}
            nextPageText={<FontAwesomeIcon icon={["fas", "angle-right"]} />}
            activePage={page}
            onChange={handlePageChange}
            itemsCountPerPage={perPage}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            itemClassNext={"!text-gray-600"}
            itemClassLast={"relative inline-flex items-center rounded-r-md px-3 py-1.5 !text-gray-600 "}
            activeClass={"relative z-10 inline-flex items-center !bg-mainColor px-3 py-1.5 text-sm font-semibold !text-white"}
            linkClassPrev={"text-gray-600"}
            itemClassFirst={"relative inline-flex items-center rounded-l-md px-3 py-1  !text-gray-600"}
            innerClass={"isolate inline-flex -space-x-px rounded-md"}
            itemClass={"relative inline-flex items-center px-3 py-1.5 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 cursor-pointer bg-white"}
        />
      </div>
  ) : null;
};

export default CustomPagination;
