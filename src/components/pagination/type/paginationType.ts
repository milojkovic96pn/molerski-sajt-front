export type PaginationType = {
    total: number;
    isEmpty: boolean;
    defaultPerPage?: number;
    isFetching?: boolean;
};