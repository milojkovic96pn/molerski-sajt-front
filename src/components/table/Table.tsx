import React, {Fragment, FunctionComponent, useCallback, useEffect, useMemo, useRef, useState} from "react";
import {AdditionalFiltersType, ColumnTypes, TableProps} from "./type";
import clsx from "clsx";
import {Modal} from "../modals/Modal";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {InputField} from "../elements/input/InputField";
import {useClickAway, useDebounce} from "react-use";
import {useTable} from "../../hooks/useTable";
import {Spinner} from "../spinner/Spinner";
import {useNavigate, useSearchParams} from "react-router-dom";
import Pagination from "../pagination/Pagination";
import { Tooltip as ReactTooltip } from "react-tooltip";
import {Button} from "../elements/button/Button";
import {CheckboxField} from "../elements/input/CheckboxField";
import {LinkItem} from "../layout/components/LinkItem";
import {ExternalLink} from "react-external-link";
import {useProgressBar} from "../../stores/progressBar";
export const Table : FunctionComponent<TableProps> = props => {

    const navigate = useNavigate();
    const { columns,data,onSearch,onAddNew,
        isLoading, additionalFilters,onDownload, pagination,
        isFetching,onCustomButton,buttonText,hideTotal,children,
        backURL,extraRow,showCountRows=true,bulkActions,
        filterClassName,onLegend } = props
    const [modalContent, setModalContent] = useState<string | boolean | null>(
        null
    );
    const [searchParams] = useSearchParams();
    const [searchValue, setSearchValue] = useState<string>("");
    const [debouncedValue, setDebouncedValue] = useState<string>("");
    const { handleSort } = useTable();
    const [openDropdown, setOpenDropdown] = useState<boolean>(false)
    const { handlePageChange } = useTable()
    const { setProgress } = useProgressBar();
    const ref = useRef(null);

    useEffect(() => {
        if (isFetching) {
            setProgress("start");
        } else {
            if (isFetching !== undefined) setProgress("finish");
        }
    }, [isFetching, setProgress]);

    useClickAway(ref, () => {
        if (openDropdown) {
            setTimeout(() => {
                setOpenDropdown(false);
            }, 100);
        }
    });

    useDebounce(
        () => {
            if (searchValue.length >= 1) {
                setDebouncedValue(searchValue);
                handlePageChange(1);
            } else setDebouncedValue(" ");
        },
        500,
        [searchValue]
    );

    useEffect(() => {
        if (onSearch && debouncedValue) onSearch(debouncedValue);
    }, [debouncedValue, onSearch]);

    const handleHideTextModal = useCallback(() => {
        setModalContent(null);
    }, []);

    const truncateField = (value: string) => {
        if (value?.toString().length > 35) {
            return (
                <span>
                     {value.toString().slice(0, 35) + "..."}
                    <i
                        className="font-bold text-sm text-mainColor cursor-pointer ml-2"
                        onClick={() => setModalContent(value)}
                    >
                        Više
                      </i>
                 </span>
            );
        }
        return value;
    };

    let currentSort = searchParams.get("sort");
    let currentDirection = searchParams.get("direction");
    const handleOrder = (fieldName: string) => {
        handleSort(fieldName, !currentDirection ? "asc"
            : fieldName !== currentSort ? "asc"
            : currentDirection === "asc" ? "desc" : ""
        )
    };

    const [filtersList, setFiltersList] = useState(columns);

    useEffect(() => {
        let cols: any[] = [...(columns || [])];
        additionalFilters?.forEach((item) => {
            let filtered = item.additionalFilters?.filter((filter1) => filter1.group);
            if (filtered?.length) {
                filtered.forEach((filter3) => {
                    cols = [...cols, ...(filter3.fields || [])];
                });
            }
            cols = [
                ...cols,
                ...(item.additionalFilters?.filter((filter2) => !filter2.group) || []),
            ];
        });
        setFiltersList(cols);
    }, [columns, additionalFilters]);

    const countRows = (style = "") => {
        return <>
            {!isLoading && data.length > 0 && !hideTotal && (
                <div className={`${style} text-[12px]`}>
                    <strong>{pagination?.to ? pagination?.to : data.length}</strong> od <strong>{pagination?.total ? pagination?.total : data.length}</strong>
                </div>
            )}
        </>
    }

    const customButtonsElement = onCustomButton?.filter((item) => item.showButton).map((item, index) => (
            <Button
                key={index}
                className={clsx(
                    "hover:opacity-100 !bg-mainBlue first:m-0 flex items-center mx-2 rounded-[10px] hover:outline outline-1 outline-mainBlue hover:!bg-white hover:text-mainBlue !font-bold",
                )}
                onClick={() => item.on()}
            >
                {item?.icon && (
                    <FontAwesomeIcon
                        icon={["fas", item.icon]}
                        className="mr-2 text-mainColor hover:cursor-pointer"
                    />
                )}
                <span className="whitespace-nowrap">
                    {item.name}{" "}
                    {item.data || item.data === 0 ? `(${item.data})` : ""}
                </span>
            </Button>
        ))

    const setSelectedRow = props?.bulkActions?.setSelectedRow;
    const selectedRow = props?.bulkActions?.selectedRow;

    const handleNewCheck = useMemo(
        () => ({ selectedRow, setSelectedRow }),
        [selectedRow, setSelectedRow]
    );

    const handleSingleCheck = (id: number) => {
        let currentValues = handleNewCheck.selectedRow?.length
            ? [...handleNewCheck?.selectedRow]
            : [];
        if (!selectedRow?.includes(id)) {
            currentValues?.push(id);
            if (handleNewCheck?.setSelectedRow) {
                handleNewCheck?.setSelectedRow(currentValues ? currentValues : []);
            }
        } else {
            if (handleNewCheck?.setSelectedRow) {
                handleNewCheck?.setSelectedRow(
                    currentValues?.filter(
                        (selectedItem: number) => selectedItem !== id
                    )
                );
            }
        }
    };

    const selectAll = () => {
       if(data.length === bulkActions?.selectedRow.length) {
           props?.bulkActions?.setSelectedRow([]);
       } else {
           props?.bulkActions?.setSelectedRow(
               data.map((item) => {
                   return item.id
               })
           );
       }
    };

    return (
        <>
            <div className="md-min:flex flex-wrap md-min:-mx-3 w-full">
                <div className="flex-none w-full max-w-full md:px-3">
                   <div>
                   {backURL || onCustomButton || onSearch || onAddNew || onCustomButton || onDownload || onLegend?
                       <div className={"flex md-max:flex-col md-min:justify-between md-max:justify-center items-center gap-4 pb-3"}>
                           {backURL || onCustomButton ?
                               <div className={"flex items-center"}>
                                   {backURL &&
                                       <div>
                                           <FontAwesomeIcon
                                               id={"return-back"}
                                               onClick={() => navigate(backURL)}
                                               icon={["fas", "arrow-left"]}
                                               className="text-3xl lg:text-4xl md-min:mr-4 md-max:mt-4 hover:cursor-pointer bg-mainBlue text-white rounded-full w-[20px] h-[20px] p-2"
                                           />
                                           <ReactTooltip anchorId={"return-back"}>
                                               Vrati se nazad
                                           </ReactTooltip>
                                       </div>
                                   }
                                   {onCustomButton &&
                                       <div className={"flex md-max:flex-col md-max:gap-3 justify-center items-center"}>
                                           {customButtonsElement}
                                       </div>
                                   }
                               </div> : null
                           }

                           {onSearch || onAddNew || onCustomButton || onDownload || onLegend ?
                               <div className={"ml-auto lg:flex items-center md-max:w-full"}>
                                   {onSearch ?
                                       <div
                                           className={clsx(
                                               "relative lg:flex flex-wrap items-stretch w-full transition-all rounded-lg ease",
                                               onAddNew || onCustomButton || onDownload ? "mr-3" : ""
                                           )}
                                       >
                                       <span
                                           className="z-20 text-sm ease leading-5.6 absolute -ml-px flex h-full items-center whitespace-nowrap rounded-lg rounded-tr-none
                                         rounded-br-none border border-r-0 border-transparent bg-transparent py-2 px-2.5 text-center font-normal text-slate-500 transition-all"
                                       >
                                         <FontAwesomeIcon
                                             className={"text-mainBlue"}
                                             icon={["fas", "search"]}
                                         />
                                       </span>
                                           <InputField
                                               className={"pl-9 text-sm min-w-0 !pt-2 !pb-2 lg:min-w-[250px]"}
                                               classNameContainer={"!mb-0 "}
                                               placeHolder={"Pretraži..."}
                                               inputType="text"
                                               value={searchValue}
                                               setValue={setSearchValue}
                                           />
                                       </div> : null
                                   }

                                   <div className={"flex justify-center items-center gap-2"}>
                                       {onLegend && data?.length ?
                                           <div
                                               id={"excel-button"}
                                               className={"px-5 legend-button hover:bg-white hover:text-lightRed hover:outline font-bold outline-1 outline-lightRed md:w-[91px] h-[38px] flex items-center justify-center cursor-pointer mt-5 lg:mt-0 bg-lightRed text-white rounded-[10px] py-1.5"}
                                           >
                                               <ExternalLink
                                                   className={"flex justify-center items-center"}
                                                   href={"https://www.kadrovska.app/legenda/"}
                                               >
                                                   <FontAwesomeIcon
                                                       icon={["fas", "book-open"]}
                                                       className="mr-2 text-white hover:cursor-pointer text-[20px]"
                                                   />
                                                   Legenda
                                               </ExternalLink>
                                           </div> : null
                                       }
                                       {onDownload && data?.length ?
                                           <div
                                               id={!onDownload.isLoading ? "excel-button" : ""}
                                               className={
                                               clsx(
                                                   "font-bold w-[91px] h-[38px] flex items-center justify-center mt-5 lg:mt-0 bg-[#477d53] text-white rounded-[10px] py-1.5 px-3",
                                                   onDownload.isLoading ? "opacity-40 select-none" : "hover:outline outline-1 outline-[#477d53] excel-button hover:bg-white hover:text-[#477d53] cursor-pointer"
                                                   )
                                               }
                                               onClick={() => onDownload.isLoading ? null : onDownload?.function()}
                                           >
                                               {onDownload.isLoading ?
                                                   <Spinner />
                                                   :
                                                   <>
                                                       <FontAwesomeIcon
                                                           icon={["fas", "file-excel"]}
                                                           className="mr-2 text-white hover:cursor-pointer hover:text-red text-[20px]"
                                                       />
                                                       <span className={"mt-[2px] font-bold"}>
                                                         Excel
                                                       </span>
                                                   </>
                                               }
                                               <ReactTooltip anchorId={"excel-button"}>
                                                   Excel
                                               </ReactTooltip>
                                           </div> : null
                                       }

                                       {onAddNew &&
                                           <div className={"relative"}
                                                ref={ref}
                                                onClick={() => onAddNew?.action ? onAddNew?.action() : onAddNew?.customDropdown ? setOpenDropdown(!openDropdown) : null}
                                           >
                                               <span
                                                     id={"add-button"}
                                                     className={"no-highlight add-button hover:bg-white font-bold hover:text-mainBlue hover:outline outline-1 outline-mainBlue w-full flex items-center justify-center cursor-pointer mt-5 lg:mt-0 bg-mainBlue text-white rounded-[10px] py-1.5 px-3"}
                                               >
                                                    <FontAwesomeIcon
                                                        icon={["fas", "plus-circle"]}
                                                        className="mr-2 text-white hover:cursor-pointer text-[20px]"
                                                    />
                                                    <span className={"mt-[2px] font-bold"}>
                                                        {buttonText || "Dodaj"}
                                                    </span>
                                                    <ReactTooltip anchorId={"add-button"}>
                                                       {onAddNew?.text ? onAddNew.text : "Dodaj"}
                                                    </ReactTooltip>
                                               </span>
                                               {openDropdown ?
                                                   <div
                                                       className={clsx(
                                                           "bg-white top-11 flex flex-col rounded-[5px] border border-mainBlue absolute w-max right-0 z-[99]",
                                                       )}
                                                   >
                                                       {onAddNew?.customDropdown && onAddNew?.customDropdown.map((link:{label: string,path: string,type: string,borderBottom?: boolean},index:number) =>
                                                           <Fragment key={index}>
                                                               <div
                                                                   className={clsx(
                                                                   "hover:bg-[#e9ecef] first:rounded-t-[5px] last:rounded-b-[5px] px-[10px] py-[5px] cursor-pointer",
                                                                    )}
                                                               >
                                                                   <LinkItem
                                                                       item={link}
                                                                       linkClassName={"!text-mainBlue"}
                                                                   />
                                                               </div>
                                                               {link.borderBottom ?
                                                                   <hr className="h-px my-2 bg-lightGray border-0" /> : null
                                                               }
                                                           </Fragment>
                                                       )}
                                                   </div> : null
                                               }
                                           </div>
                                       }
                                   </div>

                               </div> : null
                           }
                       </div> : null
                   }
                     <div className="flex-auto px-0 pt-0 shadow-xl">
                       <div className="p-0 overflow-x-auto">
                           {children ? children :
                               <div>
                                   <table
                                       className="items-center w-full mb-0 align-top border-collapse  text-slate-500">
                                       <thead className="align-bottom">
                                       <tr>
                                           {bulkActions ?
                                               <th className={"w-[40px] bg-mainColor border-r border-l"}>
                                                   <CheckboxField
                                                       onChange={() => selectAll()}
                                                       selected={data.length === bulkActions.selectedRow.length}
                                                       checkboxClassName={"!w-[19px] !h-[19px] -mt-7"}
                                                       iconClassName={"!text-[15px]"}
                                                   />
                                               </th> : null
                                           }
                                           {columns.map((item, index) => (
                                               <th
                                                   key={index}
                                                   className={clsx(
                                                       `bg-mainColor text-white font-bold p-[0.3rem] border-r last:border-0 text-[14px] no-highlight`,
                                                       item.sortable ? "cursor-pointer" : "cursor-default"
                                                   )}
                                                   onClick={() =>
                                                       item.sortable && handleOrder(item.field as string)
                                                   }
                                               >
                                                   {item.header}
                                                   {item.sortable && (
                                                       <FontAwesomeIcon
                                                           icon={[
                                                               "fas",
                                                               `${
                                                                   item.field === currentSort && currentDirection === ""
                                                                       ? "sort"
                                                                       :  item.field === currentSort && currentDirection ===  "asc"
                                                                           ? "sort-up"
                                                                           :  item.field === currentSort && currentDirection ===  "desc"
                                                                               ? "sort-down"
                                                                               : "sort"
                                                               }`,
                                                           ]}
                                                           className="text-sm inline-block ml-2 text-[#b35c06]"
                                                       />
                                                   )}
                                               </th>
                                           ))}
                                       </tr>
                                       </thead>
                                       <tbody>
                                       {!isLoading && data.map((item, index) => (
                                           <tr
                                               key={index}
                                               tabIndex={index}
                                               className={"odd:bg-lightOrange even:bg-white"}
                                           >
                                               {bulkActions ?
                                                   <td className={"w-[40px] border"}>
                                                       <CheckboxField
                                                           onChange={() => handleSingleCheck(item.id)}
                                                           selected={!!selectedRow?.includes(item.id)}
                                                           checkboxClassName={"!w-[19px] !h-[19px]"}
                                                           iconClassName={"!text-[15px]"}
                                                       />
                                                   </td> : null
                                               }
                                               {columns.map((column, colIndex) => (
                                                   <td
                                                       key={colIndex}
                                                       className={clsx(
                                                           `p-[0.3rem] text-[14px] text-center text-[#0A2B4A] border-r border-[#e9e9e9] first:border-l border-b`,
                                                           column.shrinkCol && "!w-[60px] !px-3"
                                                       )}
                                                   >
                                                       {column.element
                                                           ? column.element(item)
                                                           : column.field
                                                               ? truncateField(item[column.field] || "-")
                                                               : ""}
                                                   </td>
                                               ))}
                                           </tr>
                                       ))}
                                       {extraRow ? extraRow : null}
                                       </tbody>
                                   </table>
                                   {isLoading || !data.length ?
                                       <div
                                           className={"w-full flex justify-center items-center py-5 bg-lightOrange"}
                                       >
                                           <div className={"flex flex-col text-mainColor justify-center items-center gap-2"}>
                                               {isLoading ?
                                                   <Spinner
                                                       width={25}
                                                       height={25}
                                                       color={"#ff8309"}
                                                   /> : null
                                               }
                                               <span className={"mt-5"}>
                                                    {isLoading ? "Učitavanje podataka" : "Nema podataka"}
                                               </span>
                                           </div>
                                       </div> : null
                                   }
                               </div>
                           }
                       </div>
                     </div>

                     {/*{showCountRows && countRows("py-2")}*/}

                     {/*{pagination && !isLoading && data.length ? (*/}
                     {/*      <Pagination*/}
                     {/*          isEmpty={!data?.length}*/}
                     {/*          total={pagination.total}*/}
                     {/*          defaultPerPage={pagination.defaultPerPage}*/}
                     {/*          isFetching={isFetching}*/}
                     {/*      />*/}
                     {/*    ) : null*/}
                     {/*}*/}
                   </div>
                </div>
            </div>
            <Modal
                title={
                    `Naziv poslodavca`
                }
                show={modalContent as boolean}
                close={handleHideTextModal} typeModal={"center"}
            >
                {modalContent}
            </Modal>
        </>
    )
}