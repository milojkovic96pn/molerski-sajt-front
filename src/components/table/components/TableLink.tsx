import clsx from "clsx";
import React, {FunctionComponent} from "react";
import { useNavigate } from "react-router-dom";
import {linkProps} from "../type";


export const TableLink : FunctionComponent<linkProps> = props => {

  const { text,path,className } = props;
  const navigate = useNavigate();

  return (
    <span
      className={clsx(
        "cursor-pointer text-mainColor inline-block whitespace-nowrap hover:underline",
        className
      )}
      onClick={() => navigate(path || "/")}
    >
      {text || ""}
    </span>
  );
};
