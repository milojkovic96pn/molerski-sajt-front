import {IconProp} from '@fortawesome/fontawesome-svg-core'
import {IconName} from "@fortawesome/free-regular-svg-icons";

export type ColumnTypes = {
    header?: string;
    field?: string;
    element?: (value: any) => React.ReactNode;
    shrinkCol?: boolean;
    sortable?: boolean;
    filter_id?: string;
    filters?: { id: number | string; name: string; resource_type?: string }[];
    setFilters?: (value: any) => void;
    booleanType?: boolean;
    pickerValue?: string;
    setPickerValue?: (value:string) => void;
    dateFormat?: string;
};

type downloadType = {
    function: () => void;
    isLoading: boolean;
}

export type CustomButtonTypes = {
    on: () => void;
    name: string;
    icon?: IconName;
    data?: string | number;
    showButton?:boolean;
};


export type TableProps = {
    columns: ColumnTypes[];
    data: {
        [key: string]: any;
    }[];
    shrinkCol?: boolean;
    onSearch?: (value: string) => void;
    onAddNew?: {text?:string, action?: () => void, customDropdown?: {label: string,path: string,type: string,borderBottom?: boolean}[]};
    isLoading: boolean;
    isFetching?: boolean;
    additionalFilters?: AdditionalFiltersType[];
    onDownload?: downloadType;
    pagination?: {
        defaultPerPage?: number;
        total: number;
        to?:number;
    };
    onCustomButton?: CustomButtonTypes[];
    buttonText?: string;
    hideTotal?: boolean;
    children?: React.ReactNode;
    backURL?: string;
    extraRow?: React.ReactNode;
    showCountRows?: boolean;
    bulkActions?: {
        setSelectedRow: (value: number[]) => void;
        selectedRow: number[];
    } | null;
    filterClassName?: string
    onLegend?: boolean
    status?: string
};

export type AdditionalFiltersType = {
    group?: boolean;
    fields?: any[];
    header?: string;
    filter_id?: string;
    inputType?: string;
    filterType?: string;
    pickerValue?: any;
    setPickerValue?: any;
    filters?:
        | { id: number | string; name: string; resource_type?: string }[]
        | any;
    setFilters?: (value: any) => void;
    filtersData?: {
        [key: string]: any;
    };
    filterHeader?: string;
    filterWidth?: number;
    type?: "dropdown" | "datepicker" | "input";
    dateFormat?: string;
    pickerType?: "year" | "month";
    icon?: IconProp;
    additionalFilters?: AdditionalFiltersType[];
    isRadio?: boolean;
    booleanType?: boolean;
    placeholderPicker?: string
};

export type FilterValue = {
    filter_id: string;
    id: number;
    label: string;
    value: string;
};

export type linkProps = {
    text?: string;
    path?: string;
    className?: string;
};