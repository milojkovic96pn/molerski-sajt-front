export const InputFieldMessage = ({ text }: { text?: string }) => {

  return (
    <span className="text-red-600 text-sm italic no-highlight font-[500]">
      {text ? text : "* obavezno polje"}
    </span>
  );
};
