import {UseFormRegisterReturn} from "react-hook-form";

export type InputProps = {
    labelName?: string;
    inputType: string;
    className?: string;
    registration?: Partial<UseFormRegisterReturn>;
    error?: string | null;
    name?: string;
    hidden?: boolean;
    numberStep?: number | string;
    minStepNumber?: number | string;
    readOnly?: boolean;
    disabled?: boolean;
    placeHolder?: string;
    value?: string | number;
    checked?: boolean;
    copied?: boolean;
    classNameContainer?: string;
    setValue?: (value: string) => void,
    inputDivClass?: string,
    required?: boolean
    tooltip?: string
    hideNumberArrows?: boolean
};

export type textareaType = {
    labelName?: string;
    className?: string;
    registration?: Partial<UseFormRegisterReturn>;
    error?: boolean;
    value?: string | number;
    name?: string;
    defaultValue?: string;
    hidden?: boolean;
    handleChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
    onChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
    rows?:number;
    placeholder?:string;
    required?: boolean
};
