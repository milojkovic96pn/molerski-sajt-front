import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Tooltip as ReactTooltip} from "react-tooltip";
import {FunctionComponent} from "react";
import clsx from "clsx";

type typeProps = {
    label?: string
    tooltip?: string
    required?: boolean
    containerClass?: string
}
export const TitleInput:FunctionComponent<typeProps> = props => {

    const { label,tooltip,required,containerClass } = props

    return (
       label ?
           <label className={clsx(
               "text-[14px] text-mainBlue font-bold flex items-center mb-1 w-auto",
               containerClass && containerClass
              )}
           >
               {label} {required ? <span className={"text-lightRed ml-[1px]"}>*</span> : ""}
               {tooltip ?
                   <div
                       id={label}
                       className={"relative w-[20px] h-[20px] rounded-[5px] bg-mainBlue text-white flex justify-center items-center ml-1 !text-[14px]"}
                   >
                       <FontAwesomeIcon
                           icon={["fas", "question"]}
                       />
                       <ReactTooltip
                           anchorId={label}
                       >
                           {tooltip}
                       </ReactTooltip>
                   </div> : null
               }
           </label> : null
    )
}