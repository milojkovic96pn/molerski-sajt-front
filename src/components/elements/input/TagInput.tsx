import React, {FunctionComponent, useState} from "react";
import clsx from "clsx";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {InputFieldMessage} from "./InputFieldMessage";
import "../tag/style/tagStyle.css"

type PropsType = {
    tags: string[]
    setTags: (value:string[]) => void;
    labelName?: string;
    placeholder?: string;
    description?: string;
}

export const TagInput : FunctionComponent<PropsType> = props => {

    const { setTags, tags, labelName,placeholder,description } = props
    const [valueInput, setValueInput] = useState<string>("");
    const [ errorEmail, setErrorEmail ] = useState<boolean>(false)

    const addTag = (e: React.KeyboardEvent<HTMLInputElement>) => {
        e.preventDefault()

        if(!valueInput.length) {
            return
        } else {
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(valueInput)) {
                setErrorEmail(true)
            } else {
                let allTags = [...tags]
                allTags.push(valueInput)
                setTags(allTags)
                setValueInput("")
                if(errorEmail)  setErrorEmail(false)
            }
        }
    }

    const handleRemoveValue = (value: string) => {
        if(!value.length) return;
        const removedValue = tags.filter((val: string) => val !== value);
        setTags(removedValue)
    };

    const style = clsx(
        `focus:shadow-primary-outline
     text-sm leading-5.6 ease block w-full appearance-none rounded-[4px] border
     bg-white bg-clip-padding py-[2px] px-1.5 font-normal text-gray-500 outline-none border border-gray-500
     transition-all placeholder:text-gray-500 focus:outline-none h-[39px] 
     `,
    )

    return (
        <div className={"w-full"}>
            {labelName && (
                <label className="text-[14px] text-mainBlue font-bold mb-1" >
                    {labelName}
                </label>
            )}
            <div className={"border border-gray-500 rounder-xl rounded-lg px-4 py-2 bg-white"}>
                <div className={"p-1 flex flex-col gap-1 w-full"}>
                    {tags?.length
                        ? tags.map((val: any) => (
                            <span
                                key={val}
                                className="bg-mainBlue text-white rounded-[5px] text-[14px] px-2 py-1 mr-1 w-fit"
                            >
                                 {val}
                                <FontAwesomeIcon
                                    name={val}
                                    onClick={() => handleRemoveValue(val)}
                                    icon={["fas", "xmark"]}
                                    className="ml-2 hover:cursor-pointer"
                                />
                            </span>
                        )) : null
                    }
                </div>
               <div className={"py-2 tag-input"}>
                   <input
                       type={"text"}
                       placeholder={placeholder}
                       onChange={(e) => setValueInput(e.target.value)}
                       value={valueInput}
                       onKeyPress={(e) => e.key === "Enter" && addTag(e)}
                       className={style}
                   />
                   {errorEmail && (
                       <div>
                           <InputFieldMessage text={"Email nije validan"} />
                       </div>
                   )}
                   {description &&
                       <span className={"italic text-[#99999c]"}>{description}</span>
                   }
                </div>
            </div>
        </div>
    )
}