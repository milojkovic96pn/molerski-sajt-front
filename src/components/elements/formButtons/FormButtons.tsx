import React, {FunctionComponent} from "react";
import {Button} from "../button/Button";
import {useNavigate} from "react-router-dom";

type typeProps = {
    redirectPath: string
    loading: boolean
}
export const FormButtons:FunctionComponent<typeProps> = props => {

    const { redirectPath,loading } = props
    const navigate = useNavigate();

    const handleNavigate = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault()
        navigate(redirectPath)
    }

    return (
        // <div className="flex md-max:flex-col justify-between items-center gap-3 md:my-7 no-highlight border-t border-[#c6c6c6] pt-5">
        //     <div className={"md-max:order-2"}>
        //         <Button
        //             variant={"errorOutline"}
        //             className={"rounded-[5px] max-w-[150px]"}
        //             onClick={(e) => handleNavigate(e)}
        //         >
        //             Nazad
        //         </Button>
        //     </div>
        //     <div className="flex justify-end items-center gap-3 md-max:w-full">
        //         <Button
        //             className={"rounded-[5px] min-w-[150px]"}
        //             isLoading={loading}
        //         >
        //             Sačuvaj i izađi
        //         </Button>
        //         <Button
        //             className={"rounded-[5px] min-w-[150px]"}
        //             isLoading={loading}
        //         >
        //             Sačuvaj
        //         </Button>
        //     </div>
        // </div>
        <div className="flex md-max:flex-col justify-end items-center gap-3 md:my-7 no-highlight border-t border-[#c6c6c6] pt-5">
            <div className="flex justify-end items-center gap-3 md-max:w-full">
                <Button
                    variant={"errorOutline"}
                    className={"rounded-[5px] !w-[200px]"}
                    onClick={(e) => handleNavigate(e)}
                >
                    Nazad
                </Button>
                <Button
                    className={"rounded-[5px] !w-[200px]"}
                    isLoading={loading}
                >
                    Sačuvaj
                </Button>
            </div>
        </div>
    )
}