export type ValueOptions = {
    id: number | string;
    name: string;
}[];

export type DataOptions = {
    id: number;
    name: string;
};

export type DataItemOptions = {
    id: number | string;
    name: string;
};

export type FilterSelectProps = {
    data: DataOptions[];
    placeholder: string;
    handleSelected?: (value: any, type: any) => void;
    filterID?: string;
    value?: ValueOptions;
    filterWidth?: number;
    filterOptionsWitdh?: number;
    isMulti?: boolean;
    className?: string;
    styleSelect?: any;
    containerClass?: string;
};

export type PropsTypeSelect = {
    control?: any;
    name?: string;
    label?: string;
    errorText?: boolean;
    options?: {
        id?: number;
        value: string | number | ValueType;
        label: string;
        disabled?: boolean
    }[];
    disabled?: boolean;
    classNames?: string;
    defaultValueProp?: any;
    handleChangedValue?: (e: any) => void;
    handleSearchedValue?: (e: string) => void;
    noOptionsText?: string;
    registration?: any;
    type?: "category";
    defaultValue?: any;
    isClearable?: boolean;
    placeholder?: string
    required?: boolean
    tooltip?: string,
    value?: string
};

type ValueType = {
    label: string;
    value: number;
}[];