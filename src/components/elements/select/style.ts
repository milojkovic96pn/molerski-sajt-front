export const styleSelectdropdown = (errorText?:boolean,formInput?:boolean) => {

    return {
        placeholder: (styles: any) => ({
            ...styles,
            color: "#6b7280",
            fontSize: "0.875rem"
        }),
        control: (styles: any) => ({
            ...styles,
            height: "39px",
            "&:hover": {
                borderColor: "none",
            },
            boxShadow: 'none',
            borderColor:  errorText ? "#f43f5e" : "#6b7280",
        }),
        singleValue: (styles: any) => ({
            ...styles,
            fontSize: "0.875rem",
        }),
        option: (styles: any, { isSelected }: any) => ({
            ...styles,
            backgroundColor: isSelected && "#0A2B4A",
            "&:hover": {
                backgroundColor: isSelected ? "#0A2B4A" : "#8080801f",
                cursor: "pointer"
            },
            borderBottom: "1px solid white",
            ":last-of-type": {
                borderBottom: "none"
            }
        }),
        menuList: (styles: any) => ({
            ...styles,
           paddingTop: "0px",
           paddingBottom: "0px"
        }),
        menuPortal: (styles: any) => ({
            ...styles,
            zIndex: 99
        }),
    };
}