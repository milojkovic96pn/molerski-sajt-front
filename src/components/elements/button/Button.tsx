import clsx from "clsx";
import {Spinner} from "../../spinner/Spinner";
import React, {Fragment, FunctionComponent} from "react";
import {ButtonType, variantsButton} from "./type/buttonType";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Tooltip as ReactTooltip} from "react-tooltip";
export const Button : FunctionComponent<ButtonType> = props => {

  const {  type, disabled, isLoading, onClick, className,
      children, variant,editIcon,uploadIcon,listIcon,deleteIcon,tooltip } = props

  return (
      <Fragment>
          <button
              id={tooltip ? `button-${tooltip?.id}` : ""}
              disabled={!!(disabled || isLoading)}
              onClick={onClick || undefined}
              type={type || "submit"}
              className={clsx(
                  "w-full px-[16px] py-[8px] text-center text-white flex items-center justify-center text-[14px] font-normal tracking-wider",
                  variant ? `${variantsButton[variant]}` : "bg-mainColor",
                  disabled || isLoading  ? "opacity-40 select-none" : "hover:opacity-80",
                  editIcon || listIcon || deleteIcon ? "rounded-[5px] !p-1 !text-[12px] !w-[25px] !h-[25px] m-auto" : "rounded-[1.5rem]",
                  className && className
              )}
          >
              <div className={"relative"}>
                  <span
                      className={clsx(
                          isLoading ? "invisible" : ""
                      )}
                  >
                      {editIcon || listIcon || deleteIcon ?
                          <FontAwesomeIcon icon={["fas", editIcon ? "pen" : listIcon ? "users" : "trash" ]} /> :
                          <>
                            {uploadIcon ? <FontAwesomeIcon className={"mr-2"} icon={["fas", "folder-open"]} /> : null
                            }
                            {children}
                          </>
                      }
                  </span>
                  <div className={"absolute centered-axis"}>
                     {isLoading &&
                         <Spinner />
                     }
                  </div>
              </div>
          </button>
          {tooltip ?
              <ReactTooltip
                  anchorId={`button-${tooltip?.id}`}
              >
                  {tooltip?.text}
              </ReactTooltip> : null
          }
      </Fragment>
  );
};
