export type ButtonType = {
    children?: React.ReactNode;
    type?: "button" | "submit" | "reset";
    variant?: "primary" | "success" | "error" | "warning" | "errorOutline";
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    disabled?: boolean;
    isLoading?: boolean;
    editIcon?: boolean;
    uploadIcon?: boolean
    listIcon?: boolean
    deleteIcon?: boolean
    tooltip?: {id:number,text:string} | null
};

export const variantsButton = {
    primary: "bg-mainColor",
    success: "bg-darkGreen",
    error: "bg-lightRed",
    warning: "bg-lightYellow",
    errorOutline: "bg-white !text-mainColor border border-mainColor",
};
