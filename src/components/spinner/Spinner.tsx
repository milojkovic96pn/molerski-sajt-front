import React, {FunctionComponent} from "react";
import styled from "styled-components";

const SpinnerContainer = styled.div<{ width: number,height:number,color:string }>`
  .lds-ring {
    display: inline-block;
    position: relative;
    width: 15px;
    height: 15px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: ${(props) =>
            props.width ? `${props.width}px` : "15px"};
    height: ${(props) =>
            props.height ? `${props.height}px` : "15px"};
    border: 2px solid #fff;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: ${(props) =>
            props.color ? `${props.color} transparent transparent transparent` : "#fff transparent transparent transparent"};
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

type TypeProps = {
    color?: string,
    width?: number,
    height?: number
}
export const Spinner : FunctionComponent<TypeProps> = props => {

    const {color,height,width } = props

  return (
    <SpinnerContainer
        width={width as number}
        height={height as number}
        color={color as string}
        className={"flex"}
    >
        <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </SpinnerContainer>
  );
};
