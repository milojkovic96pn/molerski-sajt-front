export type LayoutProps = {
    children: React.ReactNode;
};

type dropdownLinkType = {
    path:string
    label:string
}

export type linkType = {
    path?:string
    label:string
}

export type changePassType = {
    lozinka: string
    ponovi_lozinku: string
}