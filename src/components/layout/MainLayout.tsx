import {Fragment, FunctionComponent} from "react";
import {LayoutProps} from "./type/layoutType";
import {Navigation} from "./components/Navigation";
import {Footer} from "./components/Footer";
export const MainLayout : FunctionComponent<LayoutProps> = props => {

    const { children } = props

    return (
        <Fragment>
            <main className={"bg-[#f8f8f8]"}>
                <div
                    className={`fixed w-full z-[99]`}
                >
                    <Navigation />
                </div>
                <div
                    className={"pt-[50px] mx-auto"}
                >
                    {children}
                </div>
                <Footer />
            </main>
        </Fragment>
    )
}