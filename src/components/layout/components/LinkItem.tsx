import React, {FunctionComponent} from "react";
import {NavLink} from "react-router-dom";
import styled from "styled-components";
import {linkType} from "../type/layoutType";
import clsx from "clsx";

type propsType = {
    item: any,
    dropdown?: boolean
    linkClassName?: string
}

const NavList = styled.div<{ dropdown:number }>`
  .active {
    color: #a36d5e
`;

export const LinkItem : FunctionComponent<propsType> = props => {

    const { item,dropdown,linkClassName ,} = props

    return (
        <>
            <NavList
                dropdown={dropdown ? 1 : 0}
            >
                <NavLink
                    to={item.path ? item.path : "/"}
                    className={clsx(
                        "px-[0.5rem] hover:text-[#ffffffbf] text-white text-[18px] block w-full",
                        linkClassName && linkClassName
                    )}
                >
                    {item.label}
                </NavLink>
            </NavList>
        </>
    )
}