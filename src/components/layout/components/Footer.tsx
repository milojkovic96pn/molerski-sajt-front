import {Link} from "react-router-dom";
import {authStore} from "../../../stores/authUser";


export const Footer = () => {

    const { user } = authStore()
    const links = [
        {
            label: "Početna",
            link: "/"
        },
        {
            label: "Kalkulator",
            link: "/kalkulator"
        },
        {
            label: "O nama",
            link: "o-nama"
        },
        {
            label: "Za majstore",
            link: "za-majstore"
        },
        user?.role === "admin" ?
            {
                label: "Korisnici",
                path: "korisnici"
            } : {}
    ]

    return (
        <footer className="bg-[#222222]">
            <div className="mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8">
                <div className="md:flex md:justify-between">
                    <div className="mb-6 md:mb-0 text-center">
                        <Link to={"/"} className="lg:flex items-center">
                           <span className={"text-[28px] font-[600] text-white"}>
                                molerskiradovi.me
                            </span>
                        </Link>
                        <span className={"text-left lg:block hidden text-white"}>
                            PRIVREDNA DIJELATNOST "GRADJEVINA"<br/>
                            BAJKOVINA BB, HERCEG NOVI<br/>
                            PIB: 03505910
                        </span>
                    </div>
                    <div className="lg:grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
                        <div className={""}>
                            <ul className="flex lg:block justify-center items-center gap-4 text-gray-500 dark:text-gray-400 font-medium text-center">
                                {links.map((link: any,index) =>
                                    <li key={index} className={`mb-2 text-white`}>
                                        <Link to={link.link}>
                                            {link.label}
                                        </Link>
                                    </li>
                                )}
                            </ul>
                        </div>
                    </div>
                    <span className={"text-center lg:hidden mt-[10px] w-full block text-white"}>
                            PRIVREDNA DIJELATNOST "GRADJEVINA"<br/>
                            BAJKOVINA BB, HERCEG NOVI<br/>
                            PIB: 03505910
                        </span>
                </div>
                <hr className="my-6 border-[#ffffff33] sm:mx-auto lg:my-8"/>
                <div className="sm:flex sm:items-center sm:justify-between">
                  <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">© 2024 {" "}
                      © Copyright 2023 <Link to={"https://mdizajn.omegalion.rs/"}><span className={"text-[#72c55e]"}>MDizajn</span></Link> . All rights reserved.
                  </span>
                </div>
            </div>
        </footer>
    )
}