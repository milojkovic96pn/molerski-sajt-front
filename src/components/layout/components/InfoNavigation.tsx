import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useMedia} from "react-use";
import {authStore} from "../../../stores/authUser";
import {formatTitle} from "../../../helpers/formatTitle";
import storage from "../../../utils/storage";
export const InfoNavigation = () => {

    const { clearUser } = authStore()
    const isMobile = useMedia("(max-width: 590px)");
    const { user } = authStore()
    const handleLogout = () => {
        clearUser()
        storage.clearToken()
    }

    return (
        <div className={"justify-center bg-mainBlue h-[50px] w-full border-t-[1px] border-[#ffffff4d] rounded-b-[50px]"}>
            <div className={"m-0 p-0 w-[85%] mx-auto h-full flex justify-between"}>
                <div className={"flex justify-center items-center"}>
                   <span className={"text-white md-min:text-[22px] md-max:text-[18px"}>
                       {formatTitle()}
                   </span>
                </div>
                <div className={"flex flex-row gap-8 justify-end items-center h-full"}>
                    {isMobile ? null :
                        <>
                            <div className={"text-[#ffffffcc] text-[13px] flex gap-3"}>
                                <div className={"flex justify-center items-center"}>
                                    <FontAwesomeIcon
                                        className={"text-mainColor text-[30px]"}
                                        icon={["fas", "user"]}
                                    />
                                </div>
                                <div className={"flex flex-col"}>
                                    <span>
                                     {user?.Rola}
                                    </span>
                                    <span>
                                      {user?.KorisnickoIme}
                                    </span>
                                </div>
                            </div>
                            <div className={"text-[#ffffffcc] text-[13px] flex gap-3"}>
                                <div className={"flex justify-center items-center"}>
                                    <FontAwesomeIcon
                                        className={"text-mainColor text-[30px]"}
                                        icon={["fas", "clock"]}
                                    />
                                </div>
                                <div className={"flex flex-col"}>
                            <span>
                              Vreme prijave
                            </span>
                            <span>
                              {user?.VremePrijave}
                            </span>
                                </div>
                            </div>
                        </>
                    }
                    <div
                        onClick={() => handleLogout()}
                        className={"text-mainColor text-[14px] flex gap-3 cursor-pointer"}
                    >
                        <div className={"flex justify-center items-center"}>
                            <span>
                              Odjava
                            </span>
                        </div>
                        <div className={"flex justify-center items-center"}>
                            <FontAwesomeIcon
                                className={"text-mainColor text-[30px]"}
                                icon={["fas", "right-from-bracket"]}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}