import {useForm} from "react-hook-form";
import React from "react";
import {yupResolver} from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {Button} from "../../elements/button/Button";
import {InputField} from "../../elements/input/InputField";
import {changePassType} from "../type/layoutType";
import {Modal} from "../../modals/Modal";
import {loginModalStore} from "../../../stores/loginModal";
import {useLoginUser, useRegisterUser} from "../../../features/register/api/registerApi";
export const LoginModal = () => {

    const { setLoginModal, loginModal } = loginModalStore();
    const {mutate: loginUser,isLoading, status} = useLoginUser()

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email("Netačan format email-a")
            .required("* obavezno polje"),
        password: Yup.string().required("* obavezno polje"),
    });

    const {
        register,
        handleSubmit,
        reset,
        formState: { errors },
    } = useForm<any>({
        resolver: yupResolver(validationSchema),
    });

    const handleCloseModal = () => {
        reset()
        setLoginModal(false);
    }

    const onSubmit = async (data: changePassType) => {
        loginUser({
            data:data
        },{
            onSuccess: () => {
                handleCloseModal()
            },
        })
    };

    return (
        <>
            <Modal
                show={loginModal}
                close={() => handleCloseModal()}
                title={
                    `Ulogujte se`
                }
                typeModal={"center"}
                className={"!w-[400px]"}
            >
                <form onSubmit={handleSubmit(onSubmit)} autoComplete={"off"}>
                    <InputField
                        registration={register("email")}
                        error={errors?.email?.message}
                        inputType="text"
                        placeHolder={"email"}
                        labelName={"Unesite email"}
                        required
                    />
                    <InputField
                        registration={register("password")}
                        error={errors?.password?.message}
                        type="password"
                        placeHolder={"lozinka"}
                        labelName={"Unesite lozinku"}
                        required
                    />
                    <div className={"flex gap-2 mt-5"}>
                        <Button
                            className={"rounded-[5px] min-w-[150px]"}
                            onClick={() => handleCloseModal()}
                            variant={"errorOutline"}
                        >
                            Izađi
                        </Button>
                        <Button
                            className={"rounded-[5px] min-w-[150px]"}
                            isLoading={isLoading}
                        >
                            Potvrdi
                        </Button>
                    </div>
                </form>
            </Modal>
        </>
    )
}