import React, {Fragment, useEffect, useState} from "react";
import clsx from "clsx";
import {Link, NavLink} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Modal} from "../../modals/Modal";
import {linkType} from "../type/layoutType";
import {LinkItem} from "./LinkItem";
import {useMedia} from "react-use";
import {authStore} from "../../../stores/authUser";
import storage from "../../../utils/storage";
import {loginModalStore} from "../../../stores/loginModal";
import {useLoginUser, useLogoutUser} from "../../../features/register/api/registerApi";
import {Spinner} from "../../spinner/Spinner";
export const Navigation = () => {

    const { clearUser } = authStore()
    const [mobileNav,setMobileNav] = useState<boolean>(false)
    const isMobile = useMedia("(max-width: 590px)");
    const { setLoginModal } = loginModalStore();
    const { user } = authStore()
    const {mutate: logoutUser,isLoading} = useLogoutUser()


    const handleLogout = () => {
        if(isLoading) return
        logoutUser()
        setMobileNav(false)
    }

    const handleLogin = () => {
        setLoginModal(true)
        setMobileNav(false)
    }

    const linksData =  [
        {
            label: "Početna",
            path: "/"
        },
        {
            label: "Kalkulator",
            path: "/kalkulator"
        },
        {
            label: "O nama",
            path: "o-nama"
        },
        {
            label: "Za majstore",
            path: "za-majstore"
        },
        user?.role === "admin" ?
            {
                label: "Korisnici",
                path: "korisnici"
            } :
            {},
        mobileNav ? {
            label: user ? "Izloguj se" : "Uloguj se",
            function: () => user ? handleLogout() : handleLogin(),
        } : {},
        mobileNav && user ? {
                label: `${user?.email} ${user?.paidUser === "1" ? `(Limit: ${user.limit})` : `(pretplati se)`}`,
            function: () => user ? handleLogout() : handleLogin(),
        } : {}
    ]

    useEffect(() => {
            if(mobileNav) {
                setMobileNav(false)
            }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[window.location.pathname])

    const [modal,setModal] = useState<boolean>(false)


    return (
        <div className={`justify-center bg-mainBlue h-[60px] w-full border-b border-mainColor ${isMobile ? "px-[10px]" : ""}`}>
            <div className={"m-0 p-0  container mx-auto h-full flex justify-between"}>
               <NavLink
                   className={"h-full"}
                   to={"/"}
               >
                   <div
                       className={"flex justify-center items-center h-full"}
                   >
                       <span className={"text-[28px] font-[600] text-white"}>
                           molerskiradovi.me
                       </span>
                   </div>
               </NavLink>
                <div className={"flex flex-row justify-end items-center h-full"}>
                    {isMobile ?
                        <div
                            className={"w-[30px] h-full flex justify-center items-center cursor-pointer"}
                            onClick={() => setMobileNav(!mobileNav)}
                        >
                            <FontAwesomeIcon
                                className="text-grayColor text-[25px]"
                                icon={["fas", "bars"]}
                            />
                        </div> : null
                    }
                    {isMobile ? null : linksData.map((link: any, index:number) =>
                        <LinkItem
                            key={index}
                            item={link}
                        />
                    )}
                    {!isMobile ?
                        <>
                            {user ?
                                <div className={"text-[#ffffffcc] text-[13px] flex gap-3 ml-3"}>
                                    <div className={"flex justify-center items-center"}>
                                        <FontAwesomeIcon
                                            className={"text-mainColor text-[30px]"}
                                            icon={["fas", "user"]}
                                        />
                                    </div>
                                    <div className={"flex justify-center items-center mt-[4px]"}>
                                        {user.email} {user.paidUser === "1" ? <>(Limit: {user.limit})</> : <span className={"ml-1 cursor-pointer"} onClick={() => setModal(true)}>(pretplati se)</span>}
                                    </div>
                                </div> : null
                            }
                            <div
                                onClick={() => user ? handleLogout() : handleLogin()}
                                className={"text-mainColor text-[14px] flex gap-3 cursor-pointer ml-3"}
                            >
                                <div className={"flex justify-center items-center"}>
                                    {isLoading ?
                                        <div className={"mt-1"}>
                                            <Spinner />
                                        </div>
                                        :
                                        <FontAwesomeIcon
                                            className={"text-mainColor text-[30px]"}
                                            icon={["fas", "right-from-bracket"]}
                                        />
                                    }
                                </div>
                                <div className={"flex justify-center items-center"}>
                                    <span className={"mt-[4px] text-white text-[18px]"}>
                                        {user ? "Izloguj se" : "Uloguj se"}
                                    </span>
                                </div>
                            </div>
                        </> : null
                    }
                </div>
            </div>
            <Modal
                title={
                    `Pretplata`
                }
                show={modal}
                close={() => setModal(false)}
                typeModal={"center"}
            >
                Posaljite kod na broj ili pozovite
            </Modal>
            <Modal
                className={"!bg-mainBlue pb-[4rem]"}
                show={mobileNav}
                close={() => setMobileNav(false)}
            >
                <div
                    className={"text-center mt-12 flex flex-col gap-1"}
                >
                    {linksData.map((link: any, index:number) =>
                        <Fragment
                            key={`singleLink-${index}`}
                        >
                            {link?.path ?
                                <LinkItem
                                    item={link}
                                /> :
                                <span onClick={() => link.function()} className={"px-[0.5rem] hover:text-[#ffffffbf] text-white text-[18px] block w-full"}>
                                    {link.label}
                                </span>
                            }
                            {link.label ?  <hr
                                className="opacity-10 h-px my-2 bg-gradient-to-r from-transparent via-white to-transparent"
                            /> : null}
                        </Fragment>
                    )}
                </div>
            </Modal>
        </div>
    )
}