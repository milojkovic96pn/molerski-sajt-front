import React from "react";

export type ModalProps = {
    show: boolean;
    children: React.ReactNode;
    className?: string;
    close: () => void;
    typeModal?: "center" | "side" | "top";
    title?: string | React.ReactNode;
};