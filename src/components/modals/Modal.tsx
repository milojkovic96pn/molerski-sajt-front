import React, {FunctionComponent, useEffect, useRef} from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useClickAway } from "react-use";
import clsx from "clsx";
import {ModalProps} from "./type/modalType";

const ModalContainer = styled.div<{ show: string,typemodal:string }>`
  right: ${(props) => (props.show === "show" ? "0" : "-100%")};
  transition: ${(props) => (props.typemodal === "center" ? "center 0.4s" : "right 0.2s")};
  &::-webkit-scrollbar {
    width: 6px;
    height: 6px;
  }

  &::-webkit-scrollbar-track {
    background: #0000000f;
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 20px;
    border: 1px solid #0000000f;
    background-color: #999999;
  }
`;

const Wrapper = styled.div<{ show: string }>`
  right: ${(props) => (props.show === "show" ? "0" : "-100%")};
  opacity: ${(props) => (props.show === "show" ? "100" : "0")};
  transition: right 0.2s;
  transition: opacity 0.2s;
`;

export const Modal : FunctionComponent<ModalProps> = props => {

    const { show, close, children, className, typeModal = "side", title } = props
    const ref = useRef(null);

    useClickAway(ref, () => {
        if (show) {
            setTimeout(() => {
                close();
            }, 200);
        }
    });

    useEffect(() => {
        if(show) {
            document.body.className = document.body.className + " overflow-hidden"
        } else {
            document.body.className = document.body.className.split(" ")[0]
        }
    }, [show]);

    return (
        <Wrapper
            className={clsx(
                `fixed w-screen h-screen top-0 bg-black bg-opacity-90 z-[99]`,
                typeModal === "center" && "flex justify-center items-center"
            )}
            show={show ? "show" : "hide"}
        >
            <ModalContainer
                // ref={ref}
                show={show ? "show" : "hide"}
                typemodal={typeModal}
                className={clsx(
                    `bg-clip-border break-words flex flex-col side-modal w-full md:w-1/2 lg:w-[25rem] top-0 bg-white z-90 shadow-3xl relative`,
                    typeModal === "center" ? "!overflow-unset mx-auto  max-h-[95vh] max-w-[95%] relative rounded-[5px]" :
                    typeModal === "top" ? "!block rounded-0 !w-full pt-5 overflow-y-auto overflow-x-hidden" :
                    "fixed h-screen  px-2.5 overflow-y-auto overflow-x-hidden",
                    className && className
                )}
            >
                <div
                    className={clsx(
                        `hover:opacity-90 absolute rounded-[50%] flex items-center
                    justify-center w-[25px] h-[25px] top-1 cursor-pointer bg-white text-mainColor`,
                       typeModal === "center" ? "right-[10px] mt-[10px]" : "top-3 right-5",
                    )}
                    onClick={close}
                >
                    <FontAwesomeIcon
                        className={"w-[15px] h-[15px]"}
                        icon={["fas", "times"]}
                    />
                </div>
                {title &&
                    <div className="py-3 mb-0 border-b-0 text-center bg-mainColor rounded-t-[5px]">
                        <div className="float-left text-center w-full">
                            <h5 className="mt-0 mb-0 font-medium text-lg text-left px-5 text-white">
                                {title}
                            </h5>
                        </div>
                    </div>
                }
                <div
                    className={clsx(
                        `flex-auto overflow-auto md:px-6 px-2 py-4`,
                    )}
                >
                    {show ? children : null}
                </div>
            </ModalContainer>
        </Wrapper>
    );
};
