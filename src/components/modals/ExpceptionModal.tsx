import React, {FunctionComponent, useEffect} from "react";
import {Modal} from "./Modal";
import {Button} from "../elements/button/Button";
import {useProgressBar} from "../../stores/progressBar";

type typeProps = {
    submit: () => void
    close: () => void
    show: boolean
    title: string
    description: string
    isLoading?: boolean
    status?: string
}
export const ExceptionModal:FunctionComponent<typeProps> = props => {

    const { close,show,title,description,submit,isLoading,status } = props
    const { setProgress } = useProgressBar();

    useEffect(() => {
        if(show) {
            if (status === "loading") setProgress("start");
            else {
                setProgress("finish");
                close()
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [status, setProgress]);

    return (
        <Modal
            show={show}
            close={close}
            title={title}
            typeModal={"center"}
            className={"!w-[600px]"}
        >
            <p className={"text-center text-[18px] pt-3 pb-5"}>
                {description}
            </p>
            <div className="flex justify-between items-center gap-3 no-highlight border-t border-[#c6c6c6] pt-5">
                <div>
                    <Button
                        className={"rounded-[5px] max-w-[150px]"}
                        variant={"errorOutline"}
                        onClick={close}
                    >
                        Otkaži
                    </Button>
                </div>
                <div className="flex justify-end items-center gap-3">
                    <Button
                        className={"rounded-[5px] w-[150px]"}
                        isLoading={isLoading}
                        onClick={submit}
                    >
                        Potvrdi
                    </Button>
                </div>
            </div>
        </Modal>
    )
}