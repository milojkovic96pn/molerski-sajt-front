import { AxiosError } from "axios";
import {toast} from "react-hot-toast";
const useErrorMessage = () => {
  const handleError = (error: any, mssg: string = "Error") => {

    if (error instanceof AxiosError) {
      let errorResponse = error?.response?.data || null;
      if (!errorResponse?.message && !errorResponse?.errors) {
        toast.error(mssg)
        return;
      }
      if (errorResponse?.message) {
        toast.error(errorResponse.message)
      }
      // if (errorResponse?.errors) {
      //   if (Object.keys(errorResponse.errors).length) {
      //     Object.keys(errorResponse.errors).forEach((item) => {
      //       let singleError = errorResponse.errors[0];
      //       toast.error(singleError)
      //     });
      //   }
      // }
    }
  };

  return { handleError };
};

export default useErrorMessage;
