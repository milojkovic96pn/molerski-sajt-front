import { toast } from "react-hot-toast";
const useClipboard = () => {
  const copyClipboard = (value: string) => {
    navigator.clipboard.writeText(value);
    toast.success("Uspešno kopiran tekst")
  };

  return { copyClipboard };
};

export default useClipboard;
