import { useSearchParams } from "react-router-dom";
import { useCallback } from "react";

type Props = {
  defaultPerPage?: number;
};

export const useTable = (props?: Props) => {

  const [searchParams, setSearchParams] = useSearchParams();

  let currentSort = searchParams.get("sort");
  let currentDirection = searchParams.get("direction");

  let currentPage = Number(searchParams.get("currentPage")) || 1;
  let currentPerPage = Number(searchParams.get("perPage")) || props?.defaultPerPage || 15;

  const handleSort = useCallback(
    (field: string, order: string) => {
      searchParams.delete("currentPage");
      searchParams.set("sort", order !== " " ? field : " ");
      searchParams.set("direction", order !== " " ? order : " ");
      if(order !== "") {
         setSearchParams(searchParams);
      } else {
         setSearchParams("")
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [searchParams]
  );

  const handlePageChange = useCallback(
    (value: number | string | null) => {
        if(value) {
            searchParams.set("currentPage", value.toString());
            setSearchParams(searchParams);
        } else {
            searchParams.delete("currentPage")
            setSearchParams(searchParams)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [searchParams]
  );

  const handlePerPageChange = useCallback(
    (value: number | string) => {
        searchParams.set("perPage", value.toString());
        setSearchParams(searchParams);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [searchParams]
  );


    return {
    sort: currentSort,
    direction: currentDirection,
    handleSort,
    currentPage: currentPage,
    perPage: currentPerPage,
    handlePageChange,
    handlePerPageChange,
  };
};
