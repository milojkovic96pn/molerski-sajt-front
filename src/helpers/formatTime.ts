import {format} from "date-fns";
import type { Dayjs } from 'dayjs';
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";

export const formatTime = (time: Date | Dayjs | string) => {
    return format(new Date(time?.toString()), "HH:mm:ss")
};

export const convertTimeToFullDateTime = (time:string) => {
    dayjs.extend(customParseFormat);
    // Assuming today's date for the example
    const today = new Date();

    // Extracting hours and minutes from the inputTime
    const [hours, minutes] = time.split(':').map(Number);

    // Setting the time on today's date
    today.setHours(hours, minutes);

    // Formatting the date-time string
    const formattedDateTime = new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZoneName: 'short'
    }).format(today);

    return dayjs(new Date(formattedDateTime).toString())
}