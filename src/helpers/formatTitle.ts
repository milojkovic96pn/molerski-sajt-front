export const formatTitle = () => {
    let location = window.location.pathname
    if(location.includes("poslodavci/detalji")) {
       return "Detalji poslodavca"
    } else if (location.includes("poslodavci")) {
        return "Pregled poslodavaca"
    }
    if(location === "/") {
        return  "Dobrodošli!"
    }
    if(location.includes("obavestenja")) {
        return "Obaveštenja"
    }
    if(location.includes("sistematizacija/dodaj")) {
        return "Detalji radnog mesta"
    } else if (location.includes("sistematizacija")) {
        return "Sistematizacija"
    }
    if(location.includes("kandidati/dodaj")) {
        return "Detalji kandidata"
    } else if (location.includes("kandidati")) {
        return "Pregled kandidata"
    }
    if(location.includes("zaposleni/dodaj")) {
        return "Detalji zaposlenog"
    } else if (location.includes("zaposleni")) {
        return "Pregled zaposlenih"
    }
    if(location.includes("dnevne-aktivnosti")) {
        return "Dnevne aktivnosti"
    }
    if(location.includes("raspored-rada")) {
        return "Raspored rada"
    }
    if(location.includes("karnet-satnice")) {
        return "Karnet i satnice"
    }
    if(location.includes("plan-rada")) {
        return "Plan rada"
    }
    if(location.includes("izvestaji")) {
        return "Izveštaji"
    }
    if(location.includes("lista-korisnika")) {
        return "Lista korisnika"
    }
    if(location.includes("lista-grupa")) {
        return "Lista grupa"
    }
};