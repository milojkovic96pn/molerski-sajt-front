import {format} from "date-fns";
export const formatDateBe = (date: string) => {
  const initialDate = new Date(
      parseInt(date.split('.')[2]),
      parseInt(date.split('.')[1]) - 1,
      parseInt(date.split('.')[0])
  );

 return new Date(`${initialDate.getFullYear()}-${(initialDate.getMonth() + 1).toString().padStart(2, '0')}-${initialDate.getDate().toString().padStart(2, '0')}`);
};
export const formatDateOriginal = (date: Date | string) => {
   return format(new Date(date), "yyyy-MM-dd")
};