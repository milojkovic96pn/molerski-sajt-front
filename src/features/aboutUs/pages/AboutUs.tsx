import {CardCalculator} from "../components/CardCalculator";
import {useMedia} from "react-use";

export const AboutUs = () => {

    const isMobile = useMedia("(max-width: 590px)");

    return (
        <div className={"bg-[#c5dce4] w-full h-full"}>
            <div className={`container mx-auto h-full gap-5 text-center flex flex-col ${isMobile ? "" : "py-[50px]"}`}>
                <CardCalculator
                    containerClassName={"!p-0"}
                    title={
                        <h1 className={"text-black text-[35px] text-white"}>
                            O nama
                        </h1>
                    }
                >
                    <div className={"p-[24px] bg-[#fff9f9] w-full mx-auto text-black container"}>
                        <br/>
                        <p className={"text-[23px]"}>
                            Mi smo tim stručnjaka koji se bavi mašinskim malterisanjem. <br/>
                            Naša kompanija ima višegodišnje iskustvo u ovoj oblasti i ponosimo se pružanjem kvalitetnih usluga
                            našim klijentima. <br/>
                            Naši izvođači izvode mašinsko malterisanje u Podgorici, Nikšiću, Herceg Novom, Tivtu, Kotoru, Budvi,
                            Baru i Ulcinju. <br/><br/>
                            Naš cilj je da Vam pružimo efikasno i profesionalno mašinsko malterisanje koje će ispuniti sve Vaše
                            potrebe i očekivanja. <br/>
                            Naš tim iskusnih majstora koristi najnoviju opremu i tehnologiju kako bi osigurao visok nivo
                            preciznosti i kvaliteta u svakom projektu. <br/><br/>
                            Sa nama, možete biti sigurni da će Vaši zidovi biti savršeno izmalterisani u kratkom vremenskom
                            roku. Sposobnost naših izvođača je da brzo i kvalitetno završe mašinski malter, i time Vam
                            omogućavaju da uštedite vrijeme i energiju.
                            <br/><br/>
                            Pored toga, cijenimo važnost dobrog odnosa sa našim klijentima. Naš tim je posvećen pružanju
                            izuzetne korisničke podrške i radimo zajedno sa Vama kako bismo postigli rezultate koji su u skladu
                            sa Vašim željama i zahtevima. I od velikog nam je značaja Vaše mišljenje. Vaše Google recenzije će
                            nam mnogo pomoći u budućem radu.
                            <br/><br/>
                            <span className={"text-left lg:block hidden"}>
                            PRIVREDNA DIJELATNOST "GRADJEVINA"<br/>
                            BAJKOVINA BB, HERCEG NOVI<br/>
                            PIB: 03505910
                        </span>
                        </p> <br/>
                    </div>
                </CardCalculator>
            </div>
        </div>
    )
}