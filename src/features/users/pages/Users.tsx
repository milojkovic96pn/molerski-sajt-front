import {useMedia} from "react-use";
import {CardCalculator} from "../components/CardCalculator";
import {useAllClients, useUpdateUser, useUpdateUserUnpaid} from "../api/usersApi";
import React from "react";
import {Table} from "../../../components/table/Table";
import {Spinner} from "../../../components/spinner/Spinner";
import {InputField} from "../../../components/elements/input/InputField";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {Button} from "../../../components/elements/button/Button";
import {LimitComponent} from "../components/LimitComponent";

export const Users = () => {

    const isMobile = useMedia("(max-width: 590px)");
    const {data,isLoading,isFetching} = useAllClients()
    const {mutate: updateUser,isLoading:loadingUpdate} = useUpdateUser()
    const {mutate: updateUserUnpaid,isLoading:loadingUpdateUnpaid} = useUpdateUserUnpaid()

    const paidUser = (id:string) => {
        if(loadingUpdate) return
        updateUser(id)
    }

    const unPaidUser = (id:string) => {
        if(loadingUpdateUnpaid) return
        updateUserUnpaid(id)
    }

    const columns = [
        {
            header: "Ime i prezime",
            field: "name",
            sortable: false
        },
        {
            header: "Email",
            field: "email",
            sortable: false
        },
        {
            header: "Adresa",
            field: "address",
            sortable: false
        },
        {
            header: "Phone",
            field: "phone",
            sortable: false
        },
        {
            header: "Pretplata",
            field: "paidUser",
            element: ( values: { paidUser:string }) => (
                <div className="flex justify-start">
                    {values?.paidUser === "0" ? 'nije preplaćen' : "preplaćen"}
                </div>
            ),
            sortable: false
        },
        {
            header: "Limit",
            field: "limit",
            element: ( values: { paidUser:string,id:string, limit:string }) => (
                <div className="flex justify-center">
                  <LimitComponent id={values.id} limit={values.limit} />
                </div>
            ),
            sortable: false
        },
        {
            header: "Rola",
            field: "role",
            sortable: false
        },
        {
            header: "Akcija",
            field: "action",
            element: ( values: { id:string , paidUser:string}) => (
                <div className="flex justify-start">
                    {values?.paidUser === "0" ? <span onClick={() => paidUser(values?.id)}
                                                      className={"cursor-pointer text-mainColor"}>pretplati</span> :
                        <span onClick={() => unPaidUser(values?.id)}
                              className={"cursor-pointer text-mainColor"}>obriši pretplatu</span>
                    }
                </div>
            ),
        }
    ]

    return (
        <div className={"bg-[#c5dce4] w-full"}>
            <div className={`container mx-auto h-full gap-5 text-center flex flex-col ${isMobile ? "" : "py-[50px]"}`}>
                <CardCalculator
                    containerClassName={"!p-0"}
                    title={
                        <h1 className={"text-black text-[35px] text-white"}>
                            Lista korisnika sajta
                        </h1>
                    }
                >
                    <div className={"w-full p-[24px] w-full mx-auto text-black container"}>
                        <div className={"flex flex-col justify-start items-start gap-5 pt-[20px] m-auto w-full"}>
                            <Table
                                isFetching={isFetching}
                                isLoading={isLoading}
                                data={data || []}
                                columns={columns}
                                // pagination={{
                                //     total: 50,
                                //     to: 2
                                // }}
                            />
                        </div>
                    </div>
                </CardCalculator>
            </div>
        </div>
    )
}