import React, {FunctionComponent} from "react";
import clsx from "clsx";

type propsType = {
    children: React.ReactNode;
    title?: any;
    containerClassName?: string
};

export const CardCalculator:FunctionComponent<propsType> = props  => {

    const { title,children,containerClassName } = props

    return (
        <div
            className={"flex flex-col shadow-xl h-full w-full"}
        >
            <div
                className={"bg-mainBlue text-white text-[14px] py-[5px] px-[10px] font-bold"}
            >
                {title}
            </div>
            <div className={clsx(
                "py-[5px] px-[10px] bg-white border-l border-b border-r border-grayColor h-full",
                containerClassName && containerClassName
            )}>
                {children}
            </div>
        </div>
    )
}