import React, {FunctionComponent, useEffect, useState} from "react";
import {InputField} from "../../../components/elements/input/InputField";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import clsx from "clsx";
import {useUpdateUserLimit, useUpdateUserUnpaid} from "../api/usersApi";

export const LimitComponent:FunctionComponent<any> = props => {
    const {id,limit} = props
    const [value,setValue] = useState<any>(0)
    const {mutate: updateUser,isLoading} = useUpdateUserLimit()

    useEffect(() => {
        setValue(limit as number)
    },[limit])

    const handleUpdate = () => {
            if(isLoading) return
            updateUser({
                data: {id:id,limit:value}
            })
    }

    return (
        <div className={"flex justify-center items-center gap-5"}>
            <InputField
                labelName={""}
                setValue={(value:any) => setValue(value)}
                inputType="text"
                type={"number"}
                classNameContainer={"max-w-[70px] !mb-0"}
                value={value}
            />
            {limit !== value ?
                <FontAwesomeIcon
                    onClick={() => handleUpdate()}
                    className={clsx(
                        "text-mainColor text-[20px] cursor-pointer"
                    )}
                    icon={["fas", "check"]}
                /> : null
            }
        </div>
    )
}