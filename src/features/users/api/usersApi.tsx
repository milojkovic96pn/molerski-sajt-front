import {axios} from "../../../lib/axios";
import {useMutation, useQuery} from "react-query";
import {ExtractFnReturnType, queryClient} from "../../../lib/react-query";
import useErrorMessage from "../../../hooks/useErrorMessage";
import {toast} from "react-hot-toast";

export const getUsers = (): Promise<any> => {
    return axios.get(`/clients`);
};
type QueryDropdownFnType = typeof getUsers;
export const useAllClients = () => {
    return useQuery<ExtractFnReturnType<QueryDropdownFnType>>({
        queryKey: ["allClients"],
        queryFn: () => getUsers(),
    });
};

export const updateUser = (id:string): Promise<any> => {
    return axios.put(`/clients/${id}/mark-as-paid`);
};

export const useUpdateUser = () => {
    const { handleError } = useErrorMessage();

    return useMutation(updateUser, {
        onSuccess: (data) => {
            toast.success("Uspešno izmenjen klijent")
            queryClient.invalidateQueries("allClients");

        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};

export const updateUserUnpaid = (id:string): Promise<any> => {
    return axios.put(`/clients/${id}/mark-as-unpaid`);
};

export const useUpdateUserUnpaid = () => {
    const { handleError } = useErrorMessage();

    return useMutation(updateUserUnpaid, {
        onSuccess: (data) => {
            toast.success("Uspešno izmenjen klijent")
            queryClient.invalidateQueries("allClients");

        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};

export const updateUserLimit = ({data}: any): Promise<any> => {
    return axios.put(`/clients/${data.id}/limit`, {limit: data.limit});
};

export const useUpdateUserLimit= () => {
    const { handleError } = useErrorMessage();

    return useMutation(updateUserLimit, {
        onSuccess: (data) => {
            toast.success("Uspešno izmenjen klijent")
            queryClient.invalidateQueries("allClients");

        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};