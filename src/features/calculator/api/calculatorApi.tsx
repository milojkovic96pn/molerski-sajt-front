import {axios} from "../../../lib/axios";
import useErrorMessage from "../../../hooks/useErrorMessage";
import {useMutation} from "react-query";
import {toast} from "react-hot-toast";

export const createWork = ({data}: any): Promise<any> => {
    return axios.post(`/works`, data);
};

export const useCreateWork = () => {
    const { handleError } = useErrorMessage();

    return useMutation(createWork, {
        onSuccess: (data) => {
            toast.success("Uspešna poslat upit")
        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};