import {InputField} from "../../../components/elements/input/InputField";
import {SelectDropdown} from "../../../components/elements/select/Select";
import {TextArea} from "../../../components/elements/input/TextArea";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {useState} from "react";
import * as Yup from "yup";
import {Button} from "../../../components/elements/button/Button";
import {CheckboxField} from "../../../components/elements/input/CheckboxField";
import {CardCalculator} from "../components/CardCalculator";
import {useMedia} from "react-use";
import {useRegisterUser} from "../../register/api/registerApi";
import {useCreateWork} from "../api/calculatorApi";
import {authStore} from "../../../stores/authUser";

export const Calculator = () => {

    const {mutate: createWork,isLoading} = useCreateWork()
    const { user } = authStore()

    const validation = Yup.object().shape({
        name: Yup.string().required("* obavezno polje"),
        address: Yup.string().required("* obavezno polje"),
        phone: Yup.string().required("* obavezno polje"),
        surface: Yup.string().required("* obavezno polje"),
        type: Yup.string().required("* obavezno polje"),
        email: Yup.string()
            .email("Netačan format email-a"),
        note: Yup.string()
            .test('len', 'Maksimum 255 karaktera', (val: any) => val.toString().length < 255),
    });
    const isMobile = useMedia("(max-width: 590px)");

    const {
        register,
        handleSubmit,
        formState: { errors },
        control,
        watch,
        reset
    } = useForm<any>({
        resolver: yupResolver(validation),
    });

    const surfaceWatch = watch("surface")
    const townWatch = watch("town")

    const [input, setInput] = useState("");
    const [checkbox, setCheckbox] = useState("");

    const handleChangeCheckboxOthers = (field:string) => {
        if (checkbox === field) {
            setCheckbox("")
        } else {
            setCheckbox(field)
        }
    }

    const handleChangeCheckbox = (field:string) => {
        if (input === field) {
            setInput("")
        } else {
            setInput(field)
        }
    }

    const handleCalculate = () => {
        const price = input === "yes" ? (townWatch === undefined || townWatch === "niksic" || townWatch === "podgorica" ? 11 : 12) : (townWatch === undefined || townWatch === "niksic" || townWatch === "podgorica" ? 6 : 7)
        return Number(surfaceWatch) * price || 0
    }

    const onSubmit = async (data:any) => {
        const formatData = {...data, material: input !== "yes" ? "ne" : "da", otherWorks: checkbox, town: data.town || "Podgorica"}
        createWork({
            data:formatData
            },{
            onSuccess: () => {
                reset()
                setCheckbox("")
                setInput("")
            },
        })
    }


        return (
        <div className={"bg-[#c5dce4] w-full"}>
            <div className={`container mx-auto h-full gap-5 text-center flex flex-col ${isMobile ? "" : "py-[50px]"}`}>
                <CardCalculator
                    containerClassName={"!p-0"}
                    title={
                        <div
                            className={"container m-auto p-[24px] lg:flex justify-center items-center gap-5 text-center"}>
                            <h1 className={"text-[25px] font-[600] text-white border-b border-mainBlue mb-5 pb-4 lg:border-0 lg:mb-0 lg:pb-0"}>
                                Kalkulator cijene
                            </h1>
                            <div className={"border-2 border-white h-[55px] w-[2px] hidden lg:block"}>
                            </div>
                            <div className={"text-white text-[19px]"}>
                                <p>
                                    Molimo Vas da ispunite sve parametre upitnika. <br/>Zatim ćemo Vas u roku jednog radnog dana kontaktirati kako bismo potvrdili sve podatke.
                                </p>
                            </div>
                        </div>
                    }
                >
                    <form className={"w-full"} onSubmit={handleSubmit(onSubmit)}>
                        <div className={"lg:grid grid-cols-2 container m-auto p-[24px]"}>
                            <div className={"flex justify-center items-center m-auto"}>
                                <div className={"flex flex-col justify-start items-start gap-5 pt-[20px] w-full"}>
                                    <div className={"md:grid grid-cols-2 justify-center items-center gap-8 w-full"}>
                                        <SelectDropdown
                                            label={"Vrsta izmerene površine:"}
                                            options={
                                                [
                                                    {
                                                        label: "Površina podova",
                                                        value: "Površina podova",
                                                    },
                                                    {
                                                        label: "Površina zidova",
                                                        value: "Površina zidova"
                                                    },
                                                ]
                                            }
                                            name={"type"}
                                            control={control}
                                            classNames={"w-full"}
                                            errorText={errors?.type?.message || null}
                                        />
                                        <InputField
                                            labelName={"Ukupna površina (m²):"}
                                            registration={register("surface")}
                                            error={errors?.surface?.message || null}
                                            inputType="text"
                                            extraField={
                                                <span className={"text-black"}>
                                                   m²
                                                  </span>
                                            }
                                            type={"number"}
                                            classNameContainer={"lg:w-[250px]"}
                                        />
                                    </div>
                                    <div className={"md:grid grid-cols-2 justify-center items-center gap-8 w-full"}>
                                        <SelectDropdown
                                            label={"Grad:"}
                                            options={
                                                [
                                                    {
                                                        label: "Podgorica",
                                                        value: "Podgorica",
                                                    },
                                                    {
                                                        label: "Herceg Novi",
                                                        value: "Herceg Novi"
                                                    },
                                                    {
                                                        label: "Budva",
                                                        value: "Budva"
                                                    },
                                                    {
                                                        label: "Kotor",
                                                        value: "Kotor"
                                                    },
                                                    {
                                                        label: "Tivat",
                                                        value: "Tivat"
                                                    },
                                                    {
                                                        label: "Bar",
                                                        value: "Bar"
                                                    },
                                                    {
                                                        label: "Nikšić",
                                                        value: "Nikšić"
                                                    },
                                                    {
                                                        label: "Ulcinj",
                                                        value: "Ulcinj"
                                                    },
                                                ]
                                            }
                                            defaultValue={{
                                                label: "Podgorica",
                                                value: "Podgorica",
                                            }}
                                            name={"town"}
                                            control={control}
                                            classNames={"w-full flex flex-col gap-3 mt-4 !w-full"}
                                        />
                                    </div>
                                    <div className={"md:grid grid-cols-2 justify-center items-center gap-8"}>
                                        <SelectDropdown
                                            label={"Popravke maltera, struganje ili uklanjanje tapeta:"}
                                            options={
                                                [
                                                    {
                                                        label: "Ne treba",
                                                        value: "Ne treba",
                                                    },
                                                    {
                                                        label: "Manje (do 20% površine)",
                                                        value: "Manje (do 20% površine)"
                                                    },
                                                    {
                                                        label: "Srednje (do 50% površine)",
                                                        value: "Srednje (do 50% površine)"
                                                    },
                                                    {
                                                        label: "Veće (do 20% površine)",
                                                        value: "Veće (do 20% površine)"
                                                    },
                                                ]
                                            }
                                            name={"othersWorkPercent"}
                                            control={control}
                                            classNames={"w-full flex flex-col gap-3 mt-4"}
                                        />
                                        <div className={"flex flex-col gap-5 justify-start items-start"}>
                                            <label className="text-[19px] text-black font-[600]">
                                                Da li da izvodjač obezbijedi farbu?
                                            </label>
                                            <CheckboxField
                                                onChange={() => handleChangeCheckbox("yes")}
                                                selected={input === "yes"}
                                                label={"Da, želim"}
                                            />
                                            <CheckboxField
                                                onChange={() => handleChangeCheckbox("no")}
                                                selected={input === "no"}
                                                label={"Ne, ja ću osigurati farbu"}
                                            />
                                        </div>
                                    </div>
                                    <div className={"md:grid grid-cols-2 justify-center items-center gap-8 w-full my-3"}>
                                        <div className={"flex flex-col gap-5 justify-start items-start"}>
                                            <label className="text-[19px] text-black font-[600]">
                                                Zanimaju me sledeći radovi:
                                            </label>
                                            <CheckboxField
                                                onChange={() => handleChangeCheckboxOthers("Temeljno čišćenje nakon krečenja")}
                                                selected={checkbox === "Temeljno čišćenje nakon krečenja"}
                                                label={"Temeljno čišćenje nakon krečenja"}
                                            />
                                            <CheckboxField
                                                onChange={() => handleChangeCheckboxOthers("Premeštanje i prekrivanje nameštaja")}
                                                selected={checkbox === "Premeštanje i prekrivanje nameštaja"}
                                                label={"Premeštanje i prekrivanje nameštaja"}
                                            />
                                        </div>
                                    </div>
                                    <TextArea
                                        registration={register("note")}
                                        error={errors?.note?.message ? true : false}
                                        labelName={"Dodatni opis:"}
                                        rows={5}
                                    />
                                    {/*<div className={"text-black w-full text-center"}>*/}
                                    {/*    <div className={"flex justify-center items-center gap-2"}>*/}
                                    {/*  <span className={"font-bold text-black text-[28px]"}>*/}
                                    {/*    €*/}
                                    {/*</span>*/}
                                    {/*        <span className={"font-bold text-black text-[30px]"}>*/}
                                    {/*    {handleCalculate()}*/}
                                    {/*        </span>*/}
                                    {/*            </div>*/}
                                    {/*            <span className={"text-[28px] text-black"}>*/}
                                    {/*        procjena cijene:*/}
                                    {/*    </span>*/}
                                    {/*</div>*/}
                                </div>
                            </div>
                            <div className={"w-full"}>
                                <div
                                    className={"flex flex-col justify-start items-start gap-5 pt-[20px] lg:w-[500px] m-auto mt-[10px] lg:mt-[0px]"}>
                                    <h1 className={"text-[25px] text-black"}>
                                        Kontakt podaci
                                    </h1>
                                        <InputField
                                            labelName={"Ime i prezime:"}
                                            registration={register("name")}
                                            inputType="text"
                                            classNameContainer={"w-full"}
                                            error={errors?.name?.message || null}
                                        />
                                        <InputField
                                            labelName={"Email:"}
                                            registration={register("email")}
                                            inputType="text"
                                            error={errors?.email?.message || null}
                                        />
                                        <InputField
                                            labelName={"Br. telefona:"}
                                            registration={register("phone")}
                                            inputType="text"
                                            error={errors?.phone?.message || null}
                                        />
                                        <InputField
                                            labelName={"Adresa radova:"}
                                            registration={register("address")}
                                            inputType="text"
                                            error={errors?.address?.message || null}
                                        />
                                        <div className={"flex flex-col gap-1 text-black mt-[30px] lg:w-[250px] text-center"}>
                                            <Button className={"!px-[2rem] !py-[8px] !bg-mainColor"} isLoading={isLoading}>
                                                Pošalji
                                            </Button>
                                            Neobvezujući zahtjev
                                        </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </CardCalculator>
            </div>
        </div>
    )
}