import {axios} from "../../../lib/axios";
import {useMutation, useQuery} from "react-query";
import {ExtractFnReturnType} from "../../../lib/react-query";
import {toast} from "react-hot-toast";
import {createWork} from "../../calculator/api/calculatorApi";
import useErrorMessage from "../../../hooks/useErrorMessage";

export const getWorks = (): Promise<any> => {
    return axios.get(`/works`);
};
type QueryDropdownFnType = typeof getWorks;
export const useAllWorks = () => {
    return useQuery<ExtractFnReturnType<QueryDropdownFnType>>({
        queryKey: ["allWorks"],
        queryFn: () => getWorks(),
    });
};

export const storeLimit = ({data}: any): Promise<any> => {
    return axios.post(`/user-work`, data);
};

export const useStoreLimit = () => {
    const { handleError } = useErrorMessage();

    return useMutation(storeLimit, {
        onSuccess: (data) => {

        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};