import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Button} from "../../../components/elements/button/Button";
import {useNavigate} from "react-router-dom";
import {useCreateWork} from "../../calculator/api/calculatorApi";
import {useAllWorks} from "../api/dashboardApi";
import {Spinner} from "../../../components/spinner/Spinner";
import {authStore} from "../../../stores/authUser";
import {loginModalStore} from "../../../stores/loginModal";
import {format} from "date-fns";
import {Modal} from "../../../components/modals/Modal";
import {NumberWork} from "../components/NumberWork";
export const Dashboard = () => {

    const navigate = useNavigate()
    const { user } = authStore()
    const { setLoginModal } = loginModalStore();
    const {data,isLoading} = useAllWorks()

    const formatDateOriginal = (date: Date | string) => {
        return format(new Date(date), "yyyy-MM-dd")
    };

    return (
      <div>
          <div className={"bg-[#c5dce4] w-full"}>
              <div className={"container mx-auto h-full gap-5 bg-[#c5dce4] py-[50px] text-center flex flex-col"}>
                  <h1 className={"text-[50px]"}>
                      Zašto odabrati nas?
                  </h1>
                  <div className={"flex flex-col justify-start items-start m-auto gap-3"}>
                      <div className={"flex justify-center items-center gap-3 text-[25px]"}>
                          <FontAwesomeIcon
                              className={"w-[25px] h-[25px] text-mainBlue"}
                              icon={["fas", "check-circle"]}
                          />
                          Možemo početi već sutra
                      </div>
                      <div className={"flex justify-center items-center gap-3 text-[25px]"}>
                          <FontAwesomeIcon
                              className={"w-[25px] h-[25px] text-mainBlue"}
                              icon={["fas", "check-circle"]}
                          />
                          Online kalkulator cena
                      </div>
                      <div className={"flex justify-center items-center gap-3 text-[25px]"}>
                          <FontAwesomeIcon
                              className={"w-[25px] h-[25px] text-mainBlue"}
                              icon={["fas", "check-circle"]}
                          />
                          Najbolja cena u gradu
                      </div>
                  </div>
                  <Button
                      className={"rounded-[5px] !w-auto m-auto !bg-mainColor mt-5 text-[20px]"}
                      onClick={() => navigate('/kalkulator')}
                  >
                      Proverite cenu u kalkulatoru
                  </Button>
              </div>
          </div>
          <div className={"w-full"}>
              <div className={"container mx-auto h-full gap-5 py-[50px] text-center flex flex-col px-[10px]"}>
                  <h1 className={"text-[20px] pb-3"}>
                      Nekoliko zadnjih upita
                  </h1>
                  {isLoading ?
                      <div className={"bg-mainColor w-fit m-auto p-[10px]"}>
                          <Spinner />
                      </div>:
                      <div className={"border-l-8 border-mainColor flex flex-col justify-start items-start m-auto gap-5 px-4 "}>
                          {data?.length ? data.map((item:any) =>
                              <div className={"flex flex-col justify-start items-start gap-3 hover:bg-[#efefef] border-b border-[#dddddd] pb-3 cursor-pointer w-full"}>
                                  <div className={"flex flex-col justify-start items-start text-[20px] text-left"}>
                                      <b>
                                          {item.type} ({item.surface} m²)
                                      </b>
                                      {item?.note ? item.note : ""} <br/>
                                      Materijal: {item?.material === "yes" ? "Izvodjač obezbedjuje farbu" : "Klijent bezbedjuje farbu"} <br/>
                                      {item.othersWorkPercent && item.othersWorkPercent !== "Ne treba" ?
                                          <>
                                              Popravke maltera, struganje ili uklanjanje tapeta: {item.othersWorkPercent}
                                          </>
                                          : "" }
                                      {item.othersWorks  ?
                                          <>
                                              Ostali radovi: {item.othersWorks}
                                          </>
                                          : "" }
                                  </div>
                                  <div className={"text-left"}>
                                      Ime klijenta: {item?.name} <br/>
                                      <span>Adresa: <b>{item.address} ({item?.town})</b></span> <br/>
                                      <NumberWork phone={item.phone} id={item.id}/>
                                      <br/>
                                      Datum: {formatDateOriginal(item.created_at)}
                                  </div>
                              </div>
                          ) : "Trenutno ne postoje usluge"}
                      </div>
                  }
              </div>
          </div>
      </div>
    )
}