import React, {FunctionComponent, useState} from "react";
import {authStore} from "../../../stores/authUser";
import {loginModalStore} from "../../../stores/loginModal";
import {Modal} from "../../../components/modals/Modal";
import {useCreateWork} from "../../calculator/api/calculatorApi";
import {useStoreLimit} from "../api/dashboardApi";

export const NumberWork:FunctionComponent<any> = props => {
    const {phone,id} = props
    const { user } = authStore()
    const [modal,setModal] = useState<boolean>(false)
    const { setLoginModal } = loginModalStore();
    const [modalNumber,setModalNumber] = useState<any>(null)
    const {mutate: storeLimit,isLoading} = useStoreLimit()

    const handleOpenPhone = () => {
        storeLimit({
            data:{user_id:user.id,work_id:id }
        },{
            onSuccess: () => {
                setModalNumber(phone)
            },
        })
    }
    return (

        <>
             <span>
                 Br: telefona: {!user || user && user?.paidUser === "0" ? "xxx-xxx" : `${ phone.substring(0, 3)}-xxx`} {" "}
                 {user && user.paidUser === "0" ? <b onClick={() => setModal(true)}>(pretplati se)</b> : user && user.paidUser ? <b onClick={() => handleOpenPhone()}>(vidi broj)</b> : <b onClick={() => setLoginModal(true)}>(ulogujte se)</b>}</span>
            <Modal
                title={
                    `Pretplata`
                }
                show={modal}
                close={() => setModal(false)}
                typeModal={"center"}
            >
                Posaljite kod na broj ili pozovite
            </Modal>
            <Modal
                title={
                    `Broj telefona`
                }
                show={modalNumber as boolean}
                close={() => setModalNumber(null)}
                typeModal={"center"}
            >
                {modalNumber}
            </Modal>
        </>
    )
}