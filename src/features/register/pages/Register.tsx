import {CardCalculator} from "../components/CardCalculator";
import {InputField} from "../../../components/elements/input/InputField";
import {SelectDropdown} from "../../../components/elements/select/Select";
import {Button} from "../../../components/elements/button/Button";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {useState} from "react";
import * as Yup from "yup";
import {Link} from "react-router-dom";
import {useMedia} from "react-use";
import {useRegisterUser} from "../api/registerApi";

export const Register = () => {

    const isMobile = useMedia("(max-width: 590px)");
    const {mutate: registerUser,isLoading, status} = useRegisterUser()


    const validation = Yup.object().shape({
        name: Yup.string().required("* obavezno polje"),
        phone: Yup.string().required("* obavezno polje"),
        password: Yup.string().required("* obavezno polje"),
        email: Yup.string()
            .email("Netačan format email-a")
            .required("* obavezno polje"),
    });
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        watch,
        control
    } = useForm<any>({
        resolver: yupResolver(validation),
    });

    const onSubmit = async (data:any) => {
        const formatData = {...data, address: data.address || "Podgorica"}
        registerUser({
            data:formatData
        },{
            onSuccess: () => {
                reset();
            },
        })
    };

    return (
        <div className={"bg-[#c5dce4] w-full"}>
            <div className={`container mx-auto h-full gap-5 text-center flex flex-col ${isMobile ? "" : "py-[50px]"}`}>
                <CardCalculator
                    containerClassName={"!p-0"}
                    title={
                        <h1 className={"text-black text-[35px] text-white"}>
                            Registrujte se na molerskiradovi.me i postanite naš partner
                        </h1>
                    }
                >
                    <div className={"w-full p-[24px] w-full mx-auto text-black container"}>
                        <div className={"flex flex-col justify-start items-start gap-5 pt-[20px] lg:w-[500px] m-auto"}>
                            <form className={"w-full"} onSubmit={handleSubmit(onSubmit)}>
                                <InputField
                                    labelName={"Ime i prezime:"}
                                    registration={register("name")}
                                    inputType="text"
                                    classNameContainer={"w-full"}
                                    error={errors?.name?.message || null}
                                />
                                <InputField
                                    labelName={"Email:"}
                                    registration={register("email")}
                                    inputType="text"
                                    error={errors?.email?.message || null}
                                />
                                <InputField
                                    labelName={"Sifra:"}
                                    registration={register("password")}
                                    type="password"
                                    classNameContainer={"w-full"}
                                    error={errors?.password?.message || null}
                                />
                                <InputField
                                    labelName={"Br. telefona"}
                                    registration={register("phone")}
                                    inputType="text"
                                    error={errors?.phone?.message || null}
                                />
                                <SelectDropdown
                                    label={"Mjesto rada:"}
                                    error={errors?.address?.message || null}
                                    options={
                                        [
                                            {
                                                label: "Podgorica",
                                                value: "podgorica",
                                            },
                                            {
                                                label: "Herceg Novi",
                                                value: "herceg-novi"
                                            },
                                            {
                                                label: "Budva",
                                                value: "budva"
                                            },
                                            {
                                                label: "Kotor",
                                                value: "kotor"
                                            },
                                            {
                                                label: "Tivat",
                                                value: "tivat"
                                            },
                                            {
                                                label: "Bar",
                                                value: "bar"
                                            },
                                            {
                                                label: "Nikšić",
                                                value: "niksic"
                                            },
                                            {
                                                label: "Ulcinj",
                                                value: "ulcinj"
                                            },
                                        ]
                                    }
                                    defaultValue={{
                                        label: "Podgorica",
                                        value: "Podgorica",
                                    }}
                                    name={"address"}
                                    control={control}
                                    classNames={"w-full flex flex-col gap-3 mt-4"}
                                />
                                <div className={"flex flex-col gap-1 text-black mt-[30px] lg:w-[250px] justify-start items-start"}>
                                    <Button className={"rounded-[5px] !bg-mainColor mt-5 text-[20px]"} isLoading={isLoading}>
                                        Registrujte se
                                    </Button>
                                </div>
                            </form>
                        </div>
                    </div>
                </CardCalculator>
            </div>
            <div className={"text-center py-[40px] text-black font-bold text-[31px] px-[15px]"}>
                <Link
                    to={"https://www.google.com/search?q=molerskiradovi.me%20reviews&sca_esv=85596a7ea76cb4a1&sca_upv=1&authuser=7&sxsrf=ACQVn09vZAJ-xrJ_z3iipO1oFBIWQ_8dJg%3A1713625054741&ei=3tcjZsvXLI-T9u8PyNagiAg&si=AKbGX_oXOTjHK3vNPxrwAU4tsC2W_rsdJDrrSHpqUAOdbOh1q5z9lcriVF0Ul8lgKnkqf7bxMuTMHgS0P5dr56-nb0YYtEcF6vrqwGc90uIimPE16cXSxqhjBMJJjxlIE7Tys-r8_k3R&ictx=1&ved=2ahUKEwjuzaPohtGFAxU7gf0HHbRHDW8QyNoBKAF6BAgbEAg&fbclid=IwZXh0bgNhZW0CMTAAAR3EDubptZVDiozkH1GsGA-r_gQcgaQONyktrQaPoSTZsGRLJ9OycWSSegU_aem_AawIOZgjAIMLEdlHUNVAgFlFrnv753ja821-dAj1Wt6GQZxaq1L7zOS8YJm3RKlDhbKmaTdlnuQox6-aBaoowCW5#wptab=si:AKbGX_p4pyeyr1FGV3SWZkER8f4E52JNbrwzMHHxrDyueD8LEgRri7pgLVpKZ9rtbTQ9mNQOX9ho5UMbfuIa3TsHflb57-_BDBDsK6Tsg9UUy_5rtCnq7LE%3D"}>
                    PROVJERENI MAJSTORI - GOOGLE RECENZIJE
                </Link>
            </div>
            <div className={"bg-[#74493d] w-full py-[15px] text-white"}>
                <div className={""}>
                    <div className={"container m-auto p-[24px] flex  flex-col justify-center items-center gap-5"}>
                        <h1 className={"text-[25px] text-white"}>
                            Zašto raditi sa nama
                        </h1>
                        <div className={"text-white text-[20px] w-full"}>
                            <b>Štedimo Vaše vrijeme</b> - unaprijed sve provjeravamo s klijentom. Ne gubite vrijeme
                            obilazeći klijente, prepustite to molerskiradovi.me kako bismo Vama obavljanje posla
                            ucinili lakše! <br/><br/>
                            <b>Budite sam svoj šef</b> - ne postoji minimalni broj obavljenih poslova. Radite kada
                            Vama odgovara. Ako se dogodi da nemate vremena ili volje, jednostavno nam se obratite i
                            javićemo Vam se drugi put. Vi odlučujete koliko narudžba želite izvršiti i koliko često
                            želite raditi.
                            <br/><br/> Bez troškova registracije ili mesečnih naknada - ne rizikujete. Naknadu
                            plaćate samo za izvršene poslove. Više ne morate brinuti o pronalazku novih narudžbina -
                            sve odrađujemo umjesto Vas.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}