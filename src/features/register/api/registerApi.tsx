import {axios} from "../../../lib/axios";
import {useMutation} from "react-query";
import {toast} from "react-hot-toast";
import useErrorMessage from "../../../hooks/useErrorMessage";
import storage from "../../../utils/storage";
import {authStore} from "../../../stores/authUser";
import {jwtDecode} from "jwt-decode";
import {queryClient} from "../../../lib/react-query";

export const createUser = ({data}: any): Promise<any> => {
    return axios.post(`/register`, data);
};


export const useRegisterUser = () => {
    const { handleError } = useErrorMessage();

    return useMutation(createUser, {
        onSuccess: (data) => {
            toast.success("Uspešna registracija")
        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};

export const loginUser = ({data}: any): Promise<any> => {
    return axios.post(`/login`, data);
};


export const useLoginUser = () => {
    const { handleError } = useErrorMessage();
    const { setUser } = authStore()

    return useMutation(loginUser, {
        onSuccess: (data) => {
            storage.setToken(data.token, "");
            queryClient.invalidateQueries("allWorks");
            setUser(data.user);
            toast.success("Uspešno si se ulogovao.")
        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};

export const logoutUser = (): Promise<any> => {
    return axios.post(`/logout`);
};


export const useLogoutUser = () => {
    const { handleError } = useErrorMessage();
    const { clearUser } = authStore()

    return useMutation(logoutUser, {
        onSuccess: (data) => {
            clearUser()
            storage.clearToken()
            toast.success("Uspešno si se izlogovao.")
        },
        onError: (error: any) => {
            handleError(error, "");
        },
    });
};
